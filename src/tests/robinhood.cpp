#include "ompl/util/gpuhash/gpurobinhood/GpuRobinHood.h"
#include <iostream>
#include <chrono>

using namespace ompl;
using namespace std;


int main() {

	try {
		OpenCL::initOpenCL();

		GpuRobinHood robin;


	} catch(cl::Error& er) {
		std::cerr << "ERROR in " << __FILE__ << " (line " << __LINE__ <<
				"):\nIn function " << er.what() << " --> " << OpenCL::printError(er.err()) << std::endl;
	}

	return 0;
}