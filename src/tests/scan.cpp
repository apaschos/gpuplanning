#include <iostream>
#include <chrono>
#include <cstdlib>
#include "ompl/util/gpuscan/GpuScan.h"

using namespace ompl;
using namespace std;


int main() {

	chrono::time_point<chrono::high_resolution_clock> start, end;
	const cl_uint samples = 20;

	vector<cl_uint> sizes = { 1<<10, 1<<11, 1<<12, 1<<13, 1<<14,
				1<<15, 1<<16, 1<<17, 1<<18, 1<<19, 1<<20,
				1<<21, 1<<22, 1<<23, 1<<24, 1<<25 };


	try {
		OpenCL::initOpenCL();


		for(cl_uint sz=0; sz<sizes.size(); sz++) {
			const cl_uint size = sizes[sz];
			cl_uint *ar = new cl_uint[size];

			// fill with data
			for(cl_uint i=0; i<size; i++) {
				ar[i] = rand() % 5;
			}

			// create input and output buffer
			cl::Buffer In = OpenCL::CLBuffer<cl_uint>(CL_MEM_READ_WRITE, size);
			cl::Buffer Out = OpenCL::CLBuffer<cl_uint>(CL_MEM_READ_WRITE, size);

			GpuScan sc; // default is 16 banks
			// GpuScan sc = {32};

			start = std::chrono::system_clock::now();
			for(cl_uint s=0; s<samples; s++)
			{
				sc.scan(In, Out);
			}
			end = std::chrono::system_clock::now();

			cout << size << "\t" << (chrono::duration_cast<chrono::nanoseconds>(end - start).count() / samples)
				<< endl;

			start = std::chrono::system_clock::now();
			for(cl_uint s=0; s<samples; s++)
			{
				sc.scan(In, Out, true);
			}
			end = std::chrono::system_clock::now();

			cout << size << "\t" << (chrono::duration_cast<chrono::nanoseconds>(end - start).count() / samples)
				<< endl;

			delete[] ar;
		}
	}
	catch(cl::Error& er) {
		std::cerr << "ERROR in " << __FILE__ << " (line " << __LINE__ <<
				"):\nIn function " << er.what() << " --> " << OpenCL::printError(er.err()) << std::endl;
	}


	return 0;
}
