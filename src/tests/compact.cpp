#include <iostream>
#include <chrono>
#include <cstdlib>
#include "ompl/util/gpucompact/GpuCompact.h"

using namespace ompl;
using namespace std;


// int main(int argc, char* argv[]) {
int main() {

	chrono::time_point<chrono::high_resolution_clock> start, end;
	const cl_uint samples = 20;
	(void) samples; // to disable compiler warning

	vector<cl_uint> sizes = { 1<<10, 1<<11, 1<<12, 1<<13, 1<<14,
				1<<15, 1<<16, 1<<17, 1<<18, 1<<19, 1<<20,
				1<<21, 1<<22, 1<<23, 1<<24, 1<<25 };


	try {
		OpenCL::initOpenCL();


		const cl_uint size = 4096;

		GpuCompact cmp = GpuCompact();

		cl::Buffer Input = OpenCL::CLBuffer<cl_uint>(CL_MEM_READ_WRITE, size*2);
		cl::Buffer Uniq;
		cl::Buffer Start;
		cl::Buffer Count;
		// OpenCL::readFromFile<cl_uint>("SortedLSHKeys.txt", Input, size, 1);
		OpenCL::readFromFile<cl_uint>("KeyValues.txt", Input, size, 2);
		
		cmp.uniqueByKeys(Input, Uniq, Start, Count, 2);
		// cmp.unique(Input, Uniq);

		OpenCL::writeToFile<cl_uint>("MyUniqkeys.txt", Uniq, OpenCL::getBufferSize<cl_uint>(Uniq)/2, 2);
		OpenCL::writeToFile<cl_uint>("MyStart.txt", Start, OpenCL::getBufferSize<cl_uint>(Start), 1);
		OpenCL::writeToFile<cl_uint>("MyCount.txt", Count, OpenCL::getBufferSize<cl_uint>(Count), 1);
	}
	catch(cl::Error& er) {
		std::cerr << "ERROR in " << __FILE__ << " (line " << __LINE__ <<
				"):\nIn function " << er.what() << " --> " << OpenCL::printError(er.err()) << std::endl;
	}


	return 0;
}