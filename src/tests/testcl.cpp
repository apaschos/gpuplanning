#define __CL_ENABLE_EXCEPTIONS

#include <CL/cl.hpp>
#include <iostream>
#include <boost/tokenizer.hpp>


int main() {
	std::vector<cl::Platform> platforms;
	bool atleastonedevice = false;

	try {

        // Get available platforms
        cl::Platform::get(&platforms);
    	if(platforms.empty())
        {
        	return EXIT_FAILURE;
        }

        for(cl_uint plt=0; plt<platforms.size(); plt++)
        {
		    std::vector<cl::Device> devices;
		    cl::Context context;
	        cl_context_properties properties[3] =  { CL_CONTEXT_PLATFORM, (cl_context_properties)(platforms[plt])(), 0};
	        context = cl::Context(CL_DEVICE_TYPE_GPU, properties); // Looking only for gpus. If you want cpu or both, enter CL_DEVICE_TYPE_CPU or CL_DEVICE_TYPE_ALL accordingly
	        devices = context.getInfo<CL_CONTEXT_DEVICES>(); // Get a list of devices on this platform
	        if(!devices.empty()) {
	        	atleastonedevice = true;
	        }
		}

		if(!atleastonedevice)
        {
        	return EXIT_FAILURE;
        }

	} catch (cl::Error& er) {
        std::cerr << "ERROR: " << er.what() << " --> " << er.err() << std::endl;
        return EXIT_FAILURE;
    }
	
	return EXIT_SUCCESS;
}


std::string printDeviceType(int type)
{
    switch(type)
    {
        case(CL_DEVICE_TYPE_CPU):
            return "CPU";
        case(CL_DEVICE_TYPE_GPU):
            return "GPU";
        case(CL_DEVICE_TYPE_ACCELERATOR):
            return "ACCELERATOR";
        case(CL_DEVICE_TYPE_DEFAULT):
            return "DEFAULT";
        default:
            return "UNKNOWN";
    }
}


std::string printCacheType(int type)
{
    switch(type)
    {
        case(CL_NONE):
            return "None";
        case(CL_READ_ONLY_CACHE):
            return "Read Only";
        case(CL_READ_WRITE_CACHE):
            return "Read-Write";
        default:
            return "Unknown";
    }
}


std::string printMemoryType(int type)
{
    switch(type)
    {
        case(CL_LOCAL):
            return "Local";
        case(CL_GLOBAL):
            return "Global";
        default:
            return "Unknown";
    }
}
