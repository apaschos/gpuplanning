#include <algorithm>
#include <iostream>
#include <chrono>
#include <cstdlib>
#include "ompl/util/gpuradixsort/GpuRadixSort.h"

using namespace ompl;
using namespace std;


// int main(int argc, char* argv[]) {
int main() {

	chrono::time_point<chrono::high_resolution_clock> start, end;
	const cl_uint samples = 20;
	(void) samples; // to disable compiler warning

	vector<cl_uint> sizes = { 1<<10, 1<<11, 1<<12, 1<<13, 1<<14,
				1<<15, 1<<16, 1<<17, 1<<18, 1<<19, 1<<20,
				1<<21, 1<<22, 1<<23, 1<<24, 1<<25 };


	try {
		OpenCL::initOpenCL();


		for(cl_uint sz=0; sz<sizes.size(); sz++) {
			const cl_uint size = sizes[sz];
			// const cl_uint multiple = 8;
			cl_uint *ar = new cl_uint[size];

			// CPU Sorting ////
			ulong total = 0;
			for(cl_uint s=0; s<samples; s++)
			{
				for(cl_uint i=0; i<size; i++) {
					ar[i] = rand() % CL_UINT_MAX;
				}

				start = std::chrono::system_clock::now();
				std::stable_sort(ar, ar+size);
				end = std::chrono::system_clock::now();

				total += chrono::duration_cast<chrono::nanoseconds>(end - start).count();
			}

			cout << size << "\t" << (total / samples) << endl;


			// GPU Sorting ////
			// fill with data
			for(cl_uint i=0; i<size; i++) {
				ar[i] = rand() % CL_UINT_MAX;
			}

			// create input and output buffer
			cl::Buffer In = OpenCL::CLBuffer<cl_uint>(CL_MEM_READ_WRITE, size);
			cl::Buffer Out = OpenCL::CLBuffer<cl_uint>(CL_MEM_READ_WRITE, size);
			OpenCL::writeToBuffer(In, 0, size, ar);

			GpuRadixSort rdx;

			start = std::chrono::system_clock::now();
			for(cl_uint s=0; s<samples; s++)
			{
				rdx.sort(In, Out);
			}
			end = std::chrono::system_clock::now();

			delete[] ar;

			cout << size << "\t" << (chrono::duration_cast<chrono::nanoseconds>(end - start).count() / samples)
				<< endl;
		}

		/////// FOR TESTING CORRECTNESS ///////////////////////

		// const cl_uint size = sizes[0];

		// cl::Buffer In = OpenCL::CLBuffer<cl_uint>(CL_MEM_READ_WRITE, size);
		// cl::Buffer Out = OpenCL::CLBuffer<cl_uint>(CL_MEM_READ_WRITE, size);
		// cl::Buffer CpuOut = OpenCL::CLBuffer<cl_uint>(CL_MEM_READ_WRITE, size);

		// OpenCL::writeToBuffer(In, 0, size, ar);
		// for(cl_uint i=0; i<size; i+=multiple)
		// {
		// 	sort(ar+i, ar+i+multiple);
		// }
		
		// OpenCL::writeToBuffer(CpuOut, 0, size, ar);
		// GpuRadixSort t = GpuRadixSort(4, 2, 32);
		// // t.sort(In, Out);
		// t.multipleSort(In, Out, multiple);

		// OpenCL::writeToFile<cl_uint>("InputKeys.txt", In, size, 1);
		// OpenCL::writeToFile<cl_uint>("SortedKeys.txt", Out, size, 1);
		// OpenCL::writeToFile<cl_uint>("CorrectSortedKeys.txt", CpuOut, size, 1);
		
	}
	catch(cl::Error& er) {
		std::cerr << "ERROR in " << __FILE__ << " (line " << __LINE__ <<
				"):\nIn function " << er.what() << " --> " << OpenCL::printError(er.err()) << std::endl;
	}


	return 0;
}