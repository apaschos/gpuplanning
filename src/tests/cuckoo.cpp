#include "ompl/util/gpuhash/gpucuckoo/GpuCuckoo.h"
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <chrono>
#include <cstdlib>

using namespace ompl;
using namespace std;


// int main(int argc, char* argv[]) {
int main() {

	chrono::time_point<chrono::high_resolution_clock> start, end;
	const cl_uint samples = 20;

	vector<cl_uint> sizes = { 1<<10, 1<<11, 1<<12, 1<<13, 1<<14,
				1<<15, 1<<16, 1<<17, 1<<18, 1<<19, 1<<20,
				1<<21, 1<<22, 1<<23, 1<<24, 1<<25 };


	try {
		OpenCL::initOpenCL();


		for(cl_uint sz=0; sz<sizes.size(); sz++) {
			const cl_uint size = sizes[sz];
			cout << "Size: " << size << endl;
			cl_uint *ar = new cl_uint[size];
			
			for(cl_uint i=0; i<size; i++) {
				ar[i] = i;
			}

			GpuCuckoo c = {0.9, 0.8, 50};
			cl::Buffer b1 = OpenCL::CLBuffer<cl_uint>(CL_MEM_READ_WRITE, size);
			cl::Buffer b2 = OpenCL::CLBuffer<cl_uint>(CL_MEM_READ_WRITE, size);

			OpenCL::writeToBuffer(b1, 0, size, ar);

			for(cl_uint i=0; i<size; i++) {
				ar[i] = size;
			}
			OpenCL::writeToBuffer(b2, 0, size, ar);

			// cl_uint total = 0;
			for(cl_uint i=0; i<samples; i++)
			{
				int ct = 0;
				while(c.build(b1) && (ct++)<50);
				if(ct == 50) {
					cout << "Failed" << endl;
				}
				else {
					c.retrieve(b1, b2);
					OpenCL::readFromBuffer(b2, 0, size, ar);

					for(cl_uint i=0; i<size; i++) {
						if(ar[i] == size) {
							cout << "bad index" << endl;
							break;
						}
					}
				}
				// {
				// 	ct++;
				// }
				// total += ++ct;
			}
			// cout << (total/samples*1.0) << endl;
			// c.retrieve(b1, b2);

			// OpenCL::writeToFile<cl_uint>("Indices.txt", b2, size, 1);

			delete[] ar;
		}
	}
	catch(cl::Error& er) {
		std::cerr << "ERROR in " << __FILE__ << " (line " << __LINE__ <<
				"):\nIn function " << er.what() << " --> " << OpenCL::printError(er.err()) << std::endl;
	}


	return 0;
}