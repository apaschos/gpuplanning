#include <iostream>
#include <chrono>
#include <random>
#include <queue>
#include <cstdlib>
#include "ompl/util/gpugraph/gpubfs/GpuBFS.h"

using namespace ompl;
using namespace std;

void cpubfs(node* graph, vector<uint> &Parent, uint nodes, uint neibs, uint Vs, uint Vg);


int main(int argc, char* argv[]) {

	chrono::time_point<chrono::high_resolution_clock> start, end;
	const cl_uint samples = 20;
	node* graph;
	cl_uint nodes, vs, vg, neibs, curneibs;
	mt19937 rng;

	vector<cl_uint> sizes = { 1<<10, 1<<11, 1<<12, 1<<13, 1<<14,
				1<<15, 1<<16, 1<<17, 1<<18, 1<<19, 1<<20, 1<<21 };

	if(argc==2){
		neibs = atoi(argv[1]);
	}
	else {
		neibs = 20;
	}

	try {
		OpenCL::initOpenCL();

		for(uint sz=0; sz<sizes.size(); sz++)
		{
			nodes = sizes[sz];

			uniform_int_distribution<uint> gen(0, nodes);
			graph = new node[nodes*neibs];
			vs = gen(rng) % nodes;
			vg = gen(rng) % nodes;
			for(cl_uint i=0; i<nodes; i++)
			{
				curneibs = gen(rng) % (neibs-4) + 4; // get at least 4 neighbours

				for(cl_uint j=0; j<curneibs; j++)
				{
					graph[i*neibs+j] = { 1, (gen(rng) % nodes) };
				}
				for(cl_uint j=curneibs; j<neibs; j++)
				{
					graph[i*neibs+j].weight = CL_UINT_MAX;
				}
			}


			// CPU BFS
			vector<uint> parnt;
			start = std::chrono::high_resolution_clock::now();
			for(cl_uint s=0; s<samples; s++)
			{
				cpubfs(graph, parnt, nodes, neibs, vs, vg);
			}
			end = std::chrono::high_resolution_clock::now();
			cout << nodes << "\t" << (chrono::duration_cast<chrono::nanoseconds>(end - start).count()/samples) << "\t";


			// GPU BFS
			GpuBFS bfs = GpuBFS(neibs);
			cl::Buffer d_graph = OpenCL::CLBuffer<node>(CL_MEM_READ_WRITE, nodes*neibs);
			cl::Buffer path;

			OpenCL::writeToBuffer(d_graph, 0, nodes*neibs, graph);

			#if(0) // Testing
			const cl_uint samples = 10000;
			cl_uint sum = 0;
			cl_uint st[samples], ed[samples], success[samples];
			cl_ulong temp, tm = 0, min_tm = CL_ULONG_MAX, max_tm = 0;
			for(cl_uint i=0; i<samples; i++) {
				st[i] = rand() % nodes;
				ed[i] = rand() % nodes;
			}

			for(cl_uint i=0; i<samples; i++) {
				start = std::chrono::high_resolution_clock::now();
				success[i] = bfs.bfs(d_graph, st[i], ed[i]);
				end = std::chrono::high_resolution_clock::now();
				temp = chrono::duration_cast<chrono::microseconds>(end - start).count();
				if(temp < min_tm) min_tm = temp;
				if(temp > max_tm) max_tm = temp;
				tm += temp;
				sum += success[i];
			}
			cout << "Mean: " << (tm/samples) << endl;
			cout << "Min: " << min_tm << endl;
			cout << "Max: " << max_tm << endl;
			cout << "Success: " << sum/(samples*1.0f)*100 << "%" << endl;

			#else
			start = std::chrono::high_resolution_clock::now();
			for(cl_uint s=0; s<samples; s++)
			{
				bfs.bfs(d_graph, vs, vg);
				bfs.getPath(path, vs, vg);
			}
			end = std::chrono::high_resolution_clock::now();
			cout << (chrono::duration_cast<chrono::nanoseconds>(end - start).count()/samples) << endl;
			#endif


			delete[] graph;
		}
	}
	catch(cl::Error& er) {
		std::cerr << "ERROR in " << __FILE__ << " (line " << __LINE__ <<
				"):\nIn function " << er.what() << " --> " << OpenCL::printError(er.err()) << std::endl;
	}


	return 0;
}


void cpubfs(node* graph, vector<uint> &Parent, uint nodes, uint neibs, uint Vs, uint Vg)
{
	queue<uint> F;
	uint node;
	vector<bool> Traversed(nodes, false);
	Parent = vector<uint>(nodes, CL_UINT_MAX);
	uint offset, dist, nid;

	F.push(Vs);
	Traversed[Vs] = true;

	while(!F.empty())
	{
		node = F.front();
		F.pop();
		if(node == Vg)
		{
			return;
		}


		for(uint i=0; i<neibs; i++)
		{
			offset = node*neibs+i;
			dist = graph[offset].weight;
			nid = graph[offset].neighbour;
			if(dist == CL_UINT_MAX)
			{
				break;
			}

			if(Traversed[nid] == true)
			{
				continue;
			}
			else 
			{
				Traversed[nid] = true;
			}

			F.push(nid);
			Parent[nid] = node;
		}
	}
}
