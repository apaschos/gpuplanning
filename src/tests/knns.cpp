#include <iostream>
#include <chrono>
#include <cstdlib>
#include "ompl/datastructures/gpulsh/GpuLSH.h"
#include "ompl/util/gpurng/GpuRandomNumbers.h"

using namespace ompl;
using namespace std;


int main(int argc, char* argv[]) {
	cl_uint dims1;
	cl_uint dims2;
	cl_uint size;
	cl_uint L;
	cl_uint M;
	cl_uint W;
	const cl_uint neibs = 20;
	const cl_uint samples = 2;

	chrono::time_point<chrono::high_resolution_clock> start, end;
	vector<cl_uint> sizes = { 1<<10, 1<<11, 1<<12, 1<<13, 1<<14,
				1<<15, 1<<16, 1<<17, 1<<18, 1<<19, 1<<20/*,
				1<<21, 1<<22, 1<<23, 1<<24, 1<<25*/ };


	if(argc==6) {
		sizes = {atoi(argv[1])};
		dims2 = atoi(argv[2]);
		L = atoi(argv[3]);
		M = atoi(argv[4]);
		W = atoi(argv[5]);
	}
	else {
		dims2 = 6;
		L = 3;
		M = 3;
		W = 5000;
	}


	try {
		OpenCL::initOpenCL();


		for(cl_uint sz=0; sz<sizes.size(); sz++) {
			dims1 = sizes[sz];
			size = dims1 * dims2;

			cl::Buffer b1 = OpenCL::CLBuffer<cl_uint>(CL_MEM_READ_WRITE, size);

			GpuRNG rnd(dims1, dims2);
			rnd.setBounds(0, 1<<14);
			rnd.generateRandomData(b1);
			// OpenCL::writeToFile<cl_uint>("randomdata.txt", b1, dims1, dims2);

			config rob;
			rob.dimensions = dims2;
			rob.weights = std::vector<cl_float>(dims2, 1.0f);

			GpuLSH knn = {rob, neibs, L, M, W};

			#if(1)
			cl_uint ct =0;
			ulong totaltime = 0;
			for(cl_uint i=0; i<samples; i++)
			{
				try {
					cl::Buffer b2;
					start = std::chrono::system_clock::now();
					knn.nearestK(b1, b2);
					end = std::chrono::system_clock::now();
					totaltime += chrono::duration_cast<chrono::nanoseconds>(end - start).count();
					ct++;
					// cout << ct << endl;
				} catch(cl::Error& er) {
					cout << "Failed with error: " << OpenCL::printError(er.err()) << endl;
				}
			}
				
			if(ct) {
				cout << dims1 << "\t" << (totaltime / ct) << endl;
			}

			//////////////////////////////////////////

			#else
			const cl_uint bufsize = dims1*neibs*2;
			cl_uint* data = new cl_uint[bufsize];
			cl_double total_mean_neibs = 0.0;
			cl_double total_mean_dist = 0.0;

			cl_double mean_neibs;
			cl_double mean_dist;
			cl_uint curneibs;
			cl_uint dist;

			for(cl_uint s=0; s<samples; s++)
			{
				try {
					cl::Buffer b2;
					knn.nearestK(b1, b2);

					mean_neibs = 0.0;
					mean_dist = 0.0;

					OpenCL::readFromBuffer(b2, 0, bufsize, data);
					for(cl_uint i=0; i<dims1; i++)
					{
						curneibs = 0;
						dist = 0;
						for(cl_uint j=0; j<neibs*2; j+=2)
						{
							if(data[i*neibs*2+j] == CL_UINT_MAX)
							{
								break;
							}

							dist += data[i*neibs*2+j];
							curneibs++;
						}

						if(curneibs){
							mean_dist += dist / (curneibs*1.0f);
						}
						mean_neibs += curneibs;
					}

					total_mean_neibs += mean_neibs/(dims1*1.0f);
					total_mean_dist += mean_dist/(dims1*1.0f);
				}
				catch(cl::Error& er) {
					cout << "Program FAILURE!!!" << endl;
				}
			}
			cout << "mean_neibs: " << (total_mean_neibs/samples) << endl;
			cout << "mean_dist: " << (total_mean_dist/samples) << endl;

			delete[] data;
			#endif
		}

	}
	catch(cl::Error& er) {
		std::cerr << "ERROR in " << __FILE__ << " (line " << __LINE__ <<
				"):\nIn function " << er.what() << " --> " << OpenCL::printError(er.err()) << std::endl;
	}


	return 0;
}