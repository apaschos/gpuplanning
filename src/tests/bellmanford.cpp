#include <iostream>
#include <chrono>
#include <random>
#include <queue>
#include <cstdlib>
#include "ompl/util/gpugraph/gpubellmanford/GpuBellmanFord.h"

using namespace ompl;
using namespace std;

// void cpubellmanford(node* graph, vector<uint> &Parent, uint nodes, uint neibs, uint Vs);


int main(int argc, char* argv[]) {

	chrono::time_point<chrono::high_resolution_clock> start, end;
	const cl_uint samples = 20;
	node* graph;
	cl_uint nodes, vs, neibs, curneibs;
	mt19937 rng;

	vector<cl_uint> sizes = { 1<<10, 1<<11, 1<<12, 1<<13, 1<<14,
				1<<15, 1<<16, 1<<17, 1<<18, 1<<19, 1<<20, 1<<21 };

	if(argc==2){
		neibs = atoi(argv[1]);
	}
	else {
		neibs = 20;
	}

	try {
		OpenCL::initOpenCL();
		OpenCL::deviceQuery();
		return 0;

		for(uint sz=0; sz<1; sz++)
		// for(uint sz=0; sz<sizes.size(); sz++)
		{
			nodes = sizes[sz];

			uniform_int_distribution<uint> gen(0, nodes);
			graph = new node[nodes*neibs];
			vs = gen(rng) % nodes;
			for(cl_uint i=0; i<nodes; i++)
			{
				curneibs = gen(rng) % (neibs-4) + 4; // get at least 4 neighbours

				for(cl_uint j=0; j<curneibs; j++)
				{
					graph[i*neibs+j].weight = 1;
					graph[i*neibs+j].neighbour = gen(rng) % nodes;
				}
				for(cl_uint j=curneibs; j<neibs; j++)
				{
					graph[i*neibs+j].weight = CL_UINT_MAX;
				}
			}


			// CPU BellmanFord
			// vector<uint> parnt;
			// start = std::chrono::high_resolution_clock::now();
			// for(cl_uint s=0; s<samples; s++)
			// {
			// 	cpubellmanford(graph, parnt, nodes, neibs, vs);
			// }
			// end = std::chrono::high_resolution_clock::now();
			// cout << nodes << "\t" << (chrono::duration_cast<chrono::nanoseconds>(end - start).count()/samples) << "\t";


			// GPU BellmanFord
			GpuBellmanFord blfd = GpuBellmanFord(neibs);
			cl::Buffer d_graph = OpenCL::CLBuffer<node>(CL_MEM_READ_WRITE, nodes*neibs);
			// cl::Buffer path;

			OpenCL::writeToBuffer(d_graph, 0, nodes*neibs, graph);


			start = std::chrono::high_resolution_clock::now();
			for(cl_uint s=0; s<samples; s++)
			{
				cout << "1" << endl;
				blfd.sssp(d_graph, vs);
				// blfd.getPath(path, vs, vg);
				cout << "2" << endl;
			}
			end = std::chrono::high_resolution_clock::now();
			cout << (chrono::duration_cast<chrono::nanoseconds>(end - start).count()/samples) << endl;


			delete[] graph;
		}
	}
	catch(cl::Error& er) {
		std::cerr << "ERROR in " << __FILE__ << " (line " << __LINE__ <<
				"):\nIn function " << er.what() << " --> " << OpenCL::printError(er.err()) << std::endl;
	}


	return 0;
}


/*
void cpubellmanford(node* graph, vector<uint> &Parent, uint nodes, uint neibs, uint Vs)
{
	queue<uint> F;
	uint node;
	vector<bool> Traversed(nodes, false);
	Parent = vector<uint>(nodes, CL_UINT_MAX);
	uint offset, dist, nid;

	F.push(Vs);
	Traversed[Vs] = true;

	while(!F.empty())
	{
		node = F.front();
		F.pop();


		for(uint i=0; i<neibs; i++)
		{
			offset = node*neibs+i;
			dist = graph[offset].weight;
			nid = graph[offset].neighbour;
			if(dist == CL_UINT_MAX)
			{
				break;
			}

			if(Traversed[nid] == true)
			{
				continue;
			}
			else 
			{
				Traversed[nid] = true;
			}

			F.push(nid);
			Parent[nid] = node;
		}
	}
}*/
