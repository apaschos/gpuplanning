#include <iostream>
#include "ompl/util/opencl/OpenCL.h"


namespace ompl {

	// use an enumerator for the kernel names (instead of 0,1,...)
	typedef enum {
		DaTest
	} kernelFunctions;

	// implement your own class inheriting from OpenCL
	class Test : public OpenCL
	{
	public:

		Test() : OpenCL()
		{
			setup();
		}

		void runDaTest(cl::Buffer& d_data)
		{
			cl_uint size = getBufferSize<cl_uint>(d_data);

			// choose a kernel
			setCurrentKernel(DaTest);
 
			// specify its arguments
			setGlobalArg(d_data);
	
			// run it !
			runKernel(size);
		}


	private:
		virtual void setup(void)
		{
			std::string programSource = "\
				__kernel void test(__global uint *A)	\
				{					\
					uint gid = get_global_id(0);	\
					if(gid < SIZE)			\
						{			\
							A[gid] = 5;	\
						}			\
				}					\
			";

			programSources = { programSource };
			programParams = { "-DSIZE=1000" };
			kernelNames = { {"test"} };
		
			// call this to compile the program and generate the kernels
			OpenCL::setup();
		}
	};
}



int main()
{
	const int size = 1000;

	try
	{

		// call it BEFORE using an OpenCL class
		ompl::OpenCL::initOpenCL();


		// create a buffer (gpu memory for arrays)
		cl::Buffer d_array = ompl::OpenCL::CLBuffer<cl_uint>(ompl::CL_MEM_GPU_ONLY, size);

		// create an object of your class now
		ompl::Test ohtest;

		// run a method
		ohtest.runDaTest(d_array);

	} catch (cl::Error& er) {
		std::cerr << "ERROR: " << er.what() << " --> " << er.err() << std::endl;
		return EXIT_FAILURE;
	}

	return 0;
}
