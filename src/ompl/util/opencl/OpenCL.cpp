/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/


/*! \file OpenCL.cpp
 *  
 *  \author Andrew Paschos
 */


#include <iostream>
#include <cassert>
#include <boost/tokenizer.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include "ompl/util/opencl/OpenCL.h"


namespace ompl{

    OpenCL::OpenCL()
    {
        assert((!platform.empty() || !device.empty()) &&
            "OpenCL is not set up. Run initOpenCL() before creating any object");
    }


    void OpenCL::initOpenCL(uint platformIndex, Type devType, uint devIndx)
    {
        std::vector<cl::Device> devs; // will hold the devices found
        std::vector<cl::Platform> pltfrms; // will hold the platforms found

        // Get available platforms
        cl::Platform::get(&pltfrms);
        assert(!pltfrms.empty() && "No platforms found.");
        assert((platformIndex < pltfrms.size()) && "Wrong platform index.");
        platform.push_back(pltfrms.at(platformIndex)); // Keep only the desired one

        cl_context_properties properties[3] =  { CL_CONTEXT_PLATFORM, (cl_context_properties)(platform[0])(), 0};
        context = cl::Context(devType, properties);
        devs = context.getInfo<CL_CONTEXT_DEVICES>(); // Get a list of devices on this platform
        assert(!devs.empty() && "No devices found in this platform.");
        assert((devIndx < devs.size()) && "Wrong device index.");
        device.push_back(devs.at(devIndx)); // Keep only the desired one

        max_work_group_size = device.at(0).getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>();
        local_mem_size = (unsigned long) (device.at(0).getInfo<CL_DEVICE_LOCAL_MEM_SIZE>() / sizeof(cl_uint));
        global_mem_size = (unsigned long) (device.at(0).getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>() / sizeof(cl_uint));

        std::string vendorname = device.at(0).getInfo<CL_DEVICE_VENDOR>();
        if(vendorname.find("Advanced Micro Device") != std::string::npos) {
            vendor = AMD;
        }
        else if(vendorname.find("NVIDIA") != std::string::npos) {
            vendor = NVIDIA;
        }
        else if(vendorname.find("Intel") != std::string::npos) {
            vendor = INTEL;
        }
        else { // add more vendors if you wish...
            vendor = OTHER;
        }

        std::string extensions = device.at(0).getInfo<CL_DEVICE_EXTENSIONS>();
        boost::char_separator<char> sep(" ");
        boost::tokenizer< boost::char_separator<char> > tokens(extensions, sep);
        for (const auto& t : tokens) { // c++11 feature
            device_extensions.push_back(t);
        }
        

        // Create a command queue and use the first device
        #ifdef PROFILING
        queue = cl::CommandQueue(context, device.at(0), CL_QUEUE_PROFILING_ENABLE); // Enabling profiling of kernel
        #else
        queue = cl::CommandQueue(context, device.at(0));
        #endif
    }


    void OpenCL::setup()
    {
        assert((!programSources.empty() && !kernelNames.empty()) &&
                    "Input OpenCL programs and/or kernels are not defined.");

        // if parameters are empty, initialize it with ""
        if(programParams.empty()) {
            programParams = std::vector<std::string>(programSources.size(), "");
        }

    	for(auto it1 = programSources.begin(),it2 = programParams.begin();
                (it1 != programSources.end() && it2 != programParams.end()); ++it1, ++it2) {
            loadProgram(*it1, *it2);
        }

        int count = 0;
        for(auto &i : kernelNames) {
            for(auto &j : i) {
                createKernel(programs.at(count), j);
            }
            count++;
        }

        kernelArgCounters = std::vector<cl_uint>(kernels.size());
    }


    void OpenCL::deviceQuery()
    {

        int max_constant_args;
        unsigned long global_mem_cache_size, max_constant_buffer_size;
        std::string extensions;
        int max_compute_units;
        int address_size;
        unsigned long max_mem_alloc_size;

        std::cout << "\nQuerying device capabilities...\n" << std::endl;

        std::cout << "Platform: " << (platform[0]).getInfo<CL_PLATFORM_NAME>() << " by " << (platform[0]).getInfo<CL_PLATFORM_VENDOR>() << std::endl;
        std::cout << "Device Type: " << printDeviceType(device.at(0).getInfo<CL_DEVICE_TYPE>()) << std::endl;
        std::cout << "Name: " << device.at(0).getInfo<CL_DEVICE_NAME>() << std::endl;
        std::string vendorname = device.at(0).getInfo<CL_DEVICE_VENDOR>();
        std::cout << "Vendor: " << vendorname << std::endl;
        std::cout << "Driver: " << device.at(0).getInfo<CL_DRIVER_VERSION>() << std::endl;
        #if defined(CL_VERSION_1_2)
            std::cout << "OpenCL Version: " << device.at(0).getInfo<CL_DEVICE_OPENCL_C_VERSION>() << std::endl; // buggy in opencl version 1.0-1.1
        #endif
        std::cout << "Max Clock Frequency: " << device.at(0).getInfo<CL_DEVICE_MAX_CLOCK_FREQUENCY>() << " MHz" << std::endl;
        max_compute_units = device.at(0).getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>();
        std::cout << "Max Compute-Units/Multiprocesssors: " << max_compute_units << std::endl;
        std::cout << "Profiling Resolution: " << device.at(0).getInfo<CL_DEVICE_PROFILING_TIMER_RESOLUTION>() << " nanosec(s)" << std::endl;
        address_size = static_cast<int>( device.at(0).getInfo<CL_DEVICE_ADDRESS_BITS>() );
        std::cout << "Default Address Size: " << address_size << "bit" << std::endl;
        std::cout << (device.at(0).getInfo<CL_DEVICE_ENDIAN_LITTLE>()==1? "Little" : "Big") << " Endian" << std::endl;
        int max_work_item_dimensions = device.at(0).getInfo<CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS>();
        std::vector<std::size_t> max_work_item_sizes = device.at(0).getInfo<CL_DEVICE_MAX_WORK_ITEM_SIZES>();
        std::cout << "Max Work-Item/Thread Dimensions: ";
        for(int k=0; k<max_work_item_dimensions; k++)
            std::cout << max_work_item_sizes[k] << " ";
        std::cout << std::endl;
        std::cout << "Max Work-Group/Block Size: " << max_work_group_size << std::endl;
        std::cout << "Global Memory Size: " << (global_mem_size*sizeof(cl_uint)) << " bytes  ("
                << (global_mem_size*sizeof(cl_uint)/1048576) << " MB)" << std::endl;
        max_mem_alloc_size = (unsigned long) device.at(0).getInfo<CL_DEVICE_MAX_MEM_ALLOC_SIZE>();
        std::cout << "Max Mem Alloc Size: " << max_mem_alloc_size << " bytes  (" << (max_mem_alloc_size / 1048576) << " MB)" << std::endl;
        global_mem_cache_size = (unsigned long) device.at(0).getInfo<CL_DEVICE_GLOBAL_MEM_CACHE_SIZE>();
        std::cout << "Global Memory Cache Size: " << global_mem_cache_size << " bytes  (" << (global_mem_cache_size / 1024) << " KB)" << std::endl;
        std::cout << "Global Memory Cache Type: " << printCacheType(device.at(0).getInfo<CL_DEVICE_GLOBAL_MEM_CACHE_TYPE>()) << std::endl;
        std::cout << "Local/Shared Memory Size: " << (local_mem_size*sizeof(cl_uint)) << " bytes ("
                << (local_mem_size*sizeof(cl_uint)/1024) << " KB)" << std::endl;
        std::cout << "Local/Shared Memory Type: " << printMemoryType(device.at(0).getInfo<CL_DEVICE_LOCAL_MEM_TYPE>()) << std::endl;
        max_constant_buffer_size = (unsigned long) device.at(0).getInfo<CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE>();
        std::cout << "Constant Memory Size: " << max_constant_buffer_size << " bytes  (" << (max_constant_buffer_size/1024) << " KB)" << std::endl;
        max_constant_args = device.at(0).getInfo<CL_DEVICE_MAX_CONSTANT_ARGS>();
        std::cout << (device.at(0).getInfo<CL_DEVICE_HOST_UNIFIED_MEMORY>()==0? "Unified Memory Subsystem exists" : "Unified Memory Subsystem does NOT exist") << std::endl;
        std::cout << "Double precision " << (device.at(0).getInfo<CL_DEVICE_DOUBLE_FP_CONFIG>()==0? "NOT " : "") << "supported" << std::endl;
        std::cout << "Max Constant Args: " << max_constant_args << std::endl;
        std::cout << "Available Extensions: " << std::endl;
        for (uint ext=0; ext<device_extensions.size(); ext++) {
            std::cout << "\t" << device_extensions[ext] << std::endl;
        }
        std::cout << std::endl;
    }


    void OpenCL::loadProgram(std::string programSource, std::string params)
    {
        cl::Program prog;

        try {
            cl::Program::Sources source(1, std::make_pair(programSource.c_str(), programSource.size()));

            prog = cl::Program(context, source); // Make program of the source code in the context

            std::string build_parameters = "";
            #ifdef DEBUGGING
            build_parameters = "-g ";
            #endif
            // add the user defined parameters
            build_parameters += params;

            prog.build(device, build_parameters.c_str());

            #ifdef VERBOSE
            std::cout << "Done building kernel" << std::endl;
            std::cout << "Build Status: " << (prog.getBuildInfo<CL_PROGRAM_BUILD_STATUS>(device.at(0)) == 0?"Success":"Failure") << std::endl;
            std::cout << "Build Options: " << prog.getBuildInfo<CL_PROGRAM_BUILD_OPTIONS>(device.at(0)) << std::endl;
            #endif

            programs.push_back(prog);
        } catch (cl::Error& er) {
            if(prog.getBuildInfo<CL_PROGRAM_BUILD_STATUS>(device.at(0)) != 0) {
                std::cout << "Build failed...\n" << prog.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device.at(0)) << std::endl;
            }
            else {
                std::cerr << "ERROR\nIn function " << er.what() << " --> " << er.err() << std::endl;
            }
            std::cout << "Exiting...\n";
            throw cl::Error(er.err(), er.what());
        }
    }


    uint OpenCL::getNumKernels(void)
    {
        return kernels.size();
    }


    void OpenCL::createKernel(const cl::Program& prog, std::string kernelName)
    {
        cl::Kernel kern(prog, kernelName.c_str());

        kernels.push_back(kern);

        // #ifdef VERBOSE
        // std::cout << "Wavefront/Warp size: " << kern.getWorkGroupInfo<CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE>(devices.at(0)) << std::endl;
        // std::cout << "Kernel Local Memory size:\t" << static_cast<ulong>(kern.getWorkGroupInfo<CL_KERNEL_LOCAL_MEM_SIZE>(devices.at(0))) << std::endl;
        // std::cout << "Kernel Private Memory size:\t" << static_cast<ulong>(kern.getWorkGroupInfo<CL_KERNEL_PRIVATE_MEM_SIZE>(devices.at(0))) << std::endl;
        // #endif
    }


    void OpenCL::setCurrentKernel(uint index) {
        kernelIndex = index;
        kernelArgCounters.at(kernelIndex) = 0;
    }


    void OpenCL::setGlobalArg(cl::Buffer& arg, cl_int argindex) {
        if(argindex != -1) {
            kernels.at(kernelIndex).setArg(argindex, arg);
        }
        else {
            kernels.at(kernelIndex).setArg(kernelArgCounters.at(kernelIndex), arg);
            kernelArgCounters.at(kernelIndex)++;
        }
    }


    void OpenCL::setLocalArg(std::size_t local_size, cl_int argindex) {
        if(argindex != -1) {
            kernels.at(kernelIndex).setArg(argindex, local_size, NULL);
        }
        else {
            kernels.at(kernelIndex).setArg(kernelArgCounters.at(kernelIndex), local_size, NULL);
            kernelArgCounters.at(kernelIndex)++;
        }
    }


    void OpenCL::setConstantArg(cl_uint arg, cl_int argindex) {
        if(argindex != -1) {
            kernels.at(kernelIndex).setArg(argindex, arg);
        }
        else {
            kernels.at(kernelIndex).setArg(kernelArgCounters.at(kernelIndex), arg);
            kernelArgCounters.at(kernelIndex)++;
        }
    }


    void OpenCL::runKernel(int global_size, int local_size, bool wait)
    {
        // Run the kernel on specific ND range
        cl::NDRange global(global_size); // number of total work-items (threads)
        cl::NDRange local;
        if(local_size != 0) {
            local = cl::NDRange(local_size); // number of work-items (threads) per work-group (block)
        }
        else {
            local = cl::NullRange; // let the compiler decide what's best
        }
        
        // Run the kernel on specific ND range
        queue.enqueueNDRangeKernel(kernels.at(kernelIndex), cl::NullRange, global, local, NULL, &event);

        if(wait) {
            waitEvent();
        }
    }


    void OpenCL::waitQueue()
    {
        #if defined(CL_VERSION_1_2)
        queue.enqueueBarrierWithWaitList();
        #else
        queue.enqueueBarrier();
        #endif
    }


    #ifdef PROFILING
    cl_ulong OpenCL::getEventTime() {
        return (event.getProfilingInfo<CL_PROFILING_COMMAND_END>() - event.getProfilingInfo<CL_PROFILING_COMMAND_START>());
    }
    #endif


    void OpenCL::waitEvent()
    {
        event.wait();
    }


    std::string OpenCL::loadSource(std::string filename)
    {
        boost::filesystem::path p(filename);

        if (!boost::filesystem::exists(p)) {
            std::cerr << "File does NOT exist.\nExiting...\n";
            exit(1);
        }
        if (!boost::filesystem::is_regular_file(p)) {
            std::cerr << "Input is not a file.\nExiting...\n";
            exit(1);
        }
        if (p.extension().string() != ".cl") {
            std::cerr << "Input is not an OpenCL kernel.\nExiting...\n";
            exit(1);
        }

        // open file and read contents
        boost::filesystem::ifstream fIn(p);
        std::string sourceContents(std::istreambuf_iterator<char>(fIn), (std::istreambuf_iterator<char>()));
        fIn.close();

        return sourceContents;
    }


    cl_uint OpenCL::extendToMultiple(cl_uint input, cl_uint multiple)
    {
        cl_uint p = 1;

        for(p=1; p*multiple<input; p<<=1);

        return p*multiple;
    }


    Vendor OpenCL::getDeviceVendor()
    {
        return vendor;
    }


    std::string OpenCL::printDeviceType(int type)
    {
        switch(type)
        {
            case(CPU):
                return "CPU";
            case(GPU):
                return "GPU";
            case(ACCELERATOR):
                return "ACCELERATOR";
            case(DEFAULT):
                return "DEFAULT";
            default:
                return "UNKNOWN";
        }
    }


    std::string OpenCL::printCacheType(int type)
    {
        switch(type)
        {
            case(CL_NONE):
                return "None";
            case(CL_READ_ONLY_CACHE):
                return "Read Only";
            case(CL_READ_WRITE_CACHE):
                return "Read-Write";
            default:
                return "Unknown";
        }
    }


    std::string OpenCL::printMemoryType(int type)
    {
        switch(type)
        {
            case(CL_LOCAL):
                return "Local";
            case(CL_GLOBAL):
                return "Global";
            default:
                return "Unknown";
        }
    }


    std::string OpenCL::printError(cl_int numError)
    {
        switch(numError) {
            case(0): return "CL_SUCCESS";
            case(-1): return "CL_DEVICE_NOT_FOUND";
            case(-2): return "CL_DEVICE_NOT_AVAILABLE";
            case(-3): return "CL_COMPILER_NOT_AVAILABLE";
            case(-4): return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
            case(-5): return "CL_OUT_OF_RESOURCES";
            case(-6): return "CL_OUT_OF_HOST_MEMORY";
            case(-7): return "CL_PROFILING_INFO_NOT_AVAILABLE";
            case(-8): return "CL_MEM_COPY_OVERLAP";
            case(-9): return "CL_IMAGE_FORMAT_MISMATCH";
            case(-10): return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
            case(-11): return "CL_BUILD_PROGRAM_FAILURE";
            case(-12): return "CL_MAP_FAILURE";
            case(-13): return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
            case(-14): return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
            case(-15): return "CL_COMPILE_PROGRAM_FAILURE";
            case(-16): return "CL_LINKER_NOT_AVAILABLE";
            case(-17): return "CL_LINK_PROGRAM_FAILURE";
            case(-18): return "CL_DEVICE_PARTITION_FAILED";
            case(-19): return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";
            case(-30): return "CL_INVALID_VALUE";
            case(-31): return "CL_INVALID_DEVICE_TYPE";
            case(-32): return "CL_INVALID_PLATFORM";
            case(-33): return "CL_INVALID_DEVICE";
            case(-34): return "CL_INVALID_CONTEXT";
            case(-35): return "CL_INVALID_QUEUE_PROPERTIES";
            case(-36): return "CL_INVALID_COMMAND_QUEUE";
            case(-37): return "CL_INVALID_HOST_PTR";
            case(-38): return "CL_INVALID_MEM_OBJECT";
            case(-39): return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
            case(-40): return "CL_INVALID_IMAGE_SIZE";
            case(-41): return "CL_INVALID_SAMPLER";
            case(-42): return "CL_INVALID_BINARY";
            case(-43): return "CL_INVALID_BUILD_OPTIONS";
            case(-44): return "CL_INVALID_PROGRAM";
            case(-45): return "CL_INVALID_PROGRAM_EXECUTABLE";
            case(-46): return "CL_INVALID_KERNEL_NAME";
            case(-47): return "CL_INVALID_KERNEL_DEFINITION";
            case(-48): return "CL_INVALID_KERNEL";
            case(-49): return "CL_INVALID_ARG_INDEX";
            case(-50): return "CL_INVALID_ARG_VALUE";
            case(-51): return "CL_INVALID_ARG_SIZE";
            case(-52): return "CL_INVALID_KERNEL_ARGS";
            case(-53): return "CL_INVALID_WORK_DIMENSION";
            case(-54): return "CL_INVALID_WORK_GROUP_SIZE";
            case(-55): return "CL_INVALID_WORK_ITEM_SIZE";
            case(-56): return "CL_INVALID_GLOBAL_OFFSET";
            case(-57): return "CL_INVALID_EVENT_WAIT_LIST";
            case(-58): return "CL_INVALID_EVENT";
            case(-59): return "CL_INVALID_OPERATION";
            case(-60): return "CL_INVALID_GL_OBJECT";
            case(-61): return "CL_INVALID_BUFFER_SIZE";
            case(-62): return "CL_INVALID_MIP_LEVEL";
            case(-63): return "CL_INVALID_GLOBAL_WORK_SIZE";
            case(-64): return "CL_INVALID_PROPERTY";
            case(-65): return "CL_INVALID_IMAGE_DESCRIPTOR";
            case(-66): return "CL_INVALID_COMPILER_OPTIONS";
            case(-67): return "CL_INVALID_LINKER_OPTIONS";
            case(-68): return "CL_INVALID_DEVICE_PARTITION_COUNT";
        }
        return (std::string("Unknown error: ") + std::to_string(numError));
    }


    //static variables declaration
    std::vector<cl::Platform> OpenCL::platform;
    cl::Context OpenCL::context;
    std::vector<cl::Device> OpenCL::device;
    cl::CommandQueue OpenCL::queue;
    Vendor OpenCL::vendor;
    cl_uint OpenCL::max_work_group_size;
    cl_uint OpenCL::local_mem_size;
    cl_uint OpenCL::global_mem_size;
    std::vector<std::string> OpenCL::device_extensions;

}