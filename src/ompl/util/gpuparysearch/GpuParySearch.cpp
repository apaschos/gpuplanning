/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */


#include "ompl/util/gpuparysearch/GpuParySearch.h"


namespace ompl
{
	typedef enum {
		Search
	}kernelFunctions;


	GpuParySearch::GpuParySearch(int blocksize) : OpenCL()
	{
		this->blocksize = blocksize;
		
		setup();
	}


	void GpuParySearch::setup(void)
	{
		programSources = { loadSource("../src/ompl/util/gpuparysearch/GpuParySearch.cl") };
	    // programParams = { "" };
	    kernelNames = { {"search"} };
	
	    OpenCL::setup();
	}
    

    void GpuParySearch::search(cl::Buffer& d_SortedData, cl::Buffer& d_Keys, cl::Buffer& d_Values)
    {
    	const cl_uint input_size = getBufferSize<cl_uint>(d_Keys);

    	setCurrentKernel(Search);
    	setGlobalArg(d_SortedData);
    	setGlobalArg(d_Keys);
    	setGlobalArg(d_Values);
    	setLocalArg(blocksize+1);
    	setConstantArg(getBufferSize<cl_uint>(d_SortedData));
    	runKernel(input_size, blocksize);
    }

}