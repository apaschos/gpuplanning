/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */


__kernel void search(__global const uint* SortedData,
                __global const uint* Keys,
                __global uint* Values,
                __local uint* Cache, // size = blocksize+1
                const uint SIZE)
{
    uint lid = get_local_id(0);
    uint groupid = get_group_id(0);
    uint blocksize = get_local_size(0);
    uint key, range_length, range_start, pad;
    __local uint range_offset;


    key = Keys[groupid];
    range_length = SortedData[SIZE-1];

    if(lid == 0)
    {
        range_offset = 0;
        Cache[blocksize] = UINT_MAX;
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    while(range_length > blocksize)
    {
        pad = (range_length%blocksize) ? 1 : 0;
        range_length /= blocksize;
        range_length += pad;

        range_start = range_offset + lid * range_length;
        Cache[lid] = SortedData[range_start];
        barrier(CLK_LOCAL_MEM_FENCE);

        if(key >= Cache[lid] && key < Cache[lid+1])
        {
            range_offset = range_start;
        }

        barrier(CLK_LOCAL_MEM_FENCE);
    }

    range_start = range_offset + lid;
    if(key == SortedData[range_start])
    {
        Values[groupid] = range_start;
    }
}
