/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */

#include "ompl/util/gpuscan/GpuScan.h"
#include "ompl/util/gpubufinit/GpuBufInit.h"
#include <cassert>
#include <cmath>


namespace ompl{

    typedef enum {
        Scan,
        AddPartialSums,
        ScanConflFree
    } kernelFunctions;


    GpuScan::GpuScan(cl_uint numBanks) : OpenCL()
    {
        this->numBanks = numBanks;

        setup();
    }


    void GpuScan::setup(void)
    {
        std::string programSource = loadSource("../src/ompl/util/gpuscan/GpuScan.cl");
        programSources = { programSource, programSource };

        std::string params = "";
        params += "-DNUM_BANKS=" + std::to_string(numBanks);
        params += " -DLOG_NUM_BANKS=" + std::to_string(static_cast<uint>(log2(numBanks)));
        params += " -DZERO_BANK_CONFLICTS";
        programParams = { "", params };

        kernelNames = { {"scan", "addPartialSums"}, {"scan"} };

        OpenCL::setup();
    }


    cl_uint GpuScan::getTotal() {
        return totalsum;
    }


    void GpuScan::scan(cl::Buffer& d_Input, cl::Buffer& d_PSum, cl_bool noBankConflicts) {
        // Input buffers must be initialized

        cl_uint ArraySize;
        cl_uint newArraySize;
        cl_uint differ;
        cl_uint total_threads;
        cl_uint group_threads;

        cl::Buffer d_partSums;
        cl::Buffer d_PSumPartSums;
        cl::Buffer *d_NewInput;
        cl::Buffer *d_NewOutput;

        // check buffers for size mismatch and keep their size
        ArraySize = getBufferSize<cl_uint>(d_Input);
        // if(getBufferSize<cl_uint>(d_PSum) < ArraySize) {
        //     OMPL_ERROR("gKNN::prefixSumGeneric\n\tOutput's size should be at least Input's size");
        //     return;
        // }
        assert(getBufferSize<cl_uint>(d_PSum) >= ArraySize &&
                "gKNN::prefixSumGeneric\n\tOutput's size should be at least Input's size");

        // If input size is smaller than the max_work_group_size, extend it to a multiple of simd lane width.
        // Else extend it to a multiple of max_work_group_size
        if(ArraySize < max_work_group_size) {
            newArraySize = extendToMultiple(ArraySize, 64); // 64 is the biggest simd lane width i know (device dependent attribute)
            // newArraySize = 1 << ceillog2(ArraySize);
        }
        else {
            newArraySize = extendToMultiple(ArraySize, max_work_group_size);
        }

        differ = newArraySize - ArraySize;
        // If the input buffer doesn't have a suitable size , we extend it to have one
        // We store its data into a new (extended) buffer
        if(differ) {
            d_NewInput = CLBufferPtr<cl_uint>(CL_MEM_GPU_ONLY, newArraySize);
            d_NewOutput = CLBufferPtr<cl_uint>(CL_MEM_GPU_ONLY, newArraySize);
            copyBetweenBuffers<cl_uint>(d_Input, *d_NewInput, 0, 0, ArraySize);

            bfn.initializeBuffer(*d_NewInput, 0, ArraySize, newArraySize);
        }
        else {
            d_NewInput = &d_Input;
            d_NewOutput = &d_PSum;
        }

        // In Scan, each thread processes two input elements, so we use half the max_work_group_size threads
        bool oneblock = false; // used to check if we need more than one work groups
        total_threads = newArraySize/2;
        // Check if input is less than half the work group size
        if(total_threads <= max_work_group_size/2) {
            oneblock = true;
            group_threads = total_threads;
        }
        else {
            group_threads = max_work_group_size/2;
        }
        

        d_partSums = CLBuffer<cl_uint>(CL_MEM_READ_WRITE, (total_threads/group_threads));
        d_PSumPartSums = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, (total_threads/group_threads));

        if(noBankConflicts && (newArraySize >= max_work_group_size))
        {
            setCurrentKernel(ScanConflFree);
        }
        else
        {
            setCurrentKernel(Scan);
        }
        setGlobalArg(*d_NewInput);
        setGlobalArg(*d_NewOutput);
        setGlobalArg(d_partSums);
        setLocalArg((group_threads*2) * sizeof(cl_uint));

        runKernel(total_threads, group_threads);

        // If we have multiple work groups, we scan the partial sums from each one and add the results
        if(!oneblock) {
            scan(d_partSums, d_PSumPartSums);

            total_threads = newArraySize;
            group_threads = (total_threads < max_work_group_size) ?
                        total_threads : max_work_group_size;

            setCurrentKernel(AddPartialSums);

            setGlobalArg(*d_NewOutput);
            setGlobalArg(d_PSumPartSums);

            runKernel(total_threads, group_threads);
        }
        else {
            // We store the element from d_partSums which holds the total sum of the input array
            readFromBuffer(d_partSums, 0, 1, &totalsum);
        }

        // If the input buffer was extended, we copy back only the initial part scanned
        if(differ) {
            copyBetweenBuffers<cl_uint>(*d_NewOutput, d_PSum, 0, 0, ArraySize);
            delete d_NewInput;
            delete d_NewOutput;
        }
    }
}
