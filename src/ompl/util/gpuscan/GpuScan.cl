/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */

#ifdef ZERO_BANK_CONFLICTS
#define CONFLICT_FREE_OFFSET(n) ((n) >> NUM_BANKS + (n) >> (LOG_NUM_BANKS << 1))
#else
#define CONFLICT_FREE_OFFSET(n) ((n) >> LOG_NUM_BANKS)
#endif


__kernel void scan(__global uint* Input,
            __global uint* Output,
            __global uint* partialSums,
            __local uint* localBuffer);

__kernel void addPartialSums(__global uint *Array,
            __global const uint *psumed_partialSums);

inline void upsweep(__local uint* input,
            __global uint* partSums);

inline void downsweep(__local uint* input);


////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


__kernel void scan(__global uint* Input,
            __global uint* Output,
            __global uint* partialSums,
            __local uint* localBuffer)
{

    #ifdef ZERO_BANK_CONFLICTS
    uint lid = get_local_id(0);
    uint block_size = get_local_size(0);
    uint l_offset = lid;
    uint l_offset1 = l_offset + block_size;
    uint g_offset = get_group_id(0)*(block_size<<1) + lid;
    uint g_offset1 = g_offset + block_size;
    uint bankOffset = CONFLICT_FREE_OFFSET(lid);

    localBuffer[l_offset + bankOffset] = Input[g_offset];
    localBuffer[l_offset1 + bankOffset] = Input[g_offset1];


    #else
    uint l_offset = get_local_id(0)<<1;
    uint l_offset1 = l_offset + 1;
    uint g_offset = get_global_id(0)<<1;
    uint g_offset1 = g_offset + 1;

    localBuffer[l_offset] = Input[g_offset];
    localBuffer[l_offset1] = Input[g_offset1];
    #endif

    barrier(CLK_LOCAL_MEM_FENCE);

    upsweep(localBuffer, partialSums);
    downsweep(localBuffer);

    // COPY back to GLOBAL memory
    #ifdef ZERO_BANK_CONFLICTS
    Output[g_offset] = localBuffer[l_offset + bankOffset];
    Output[g_offset1] = localBuffer[l_offset1 + bankOffset];

    #else
    Output[g_offset] = localBuffer[l_offset];
    Output[g_offset1] = localBuffer[l_offset1];
    #endif

}


__kernel void addPartialSums(__global uint *Array,
            __global const uint *psumed_partialSums)
{
    uint gid = get_global_id(0);
    uint groupid = get_group_id(0);

    Array[gid] += psumed_partialSums[groupid];
}



inline void upsweep(__local uint* input,
            __global uint* partSums)
{
    uint lid = get_local_id(0);
    uint groupid = get_group_id(0);
    uint block_size = get_local_size(0);
    uint lofst = (lid<<1)+1;
    uint ai, bi, offset;

    offset = 1;
    for(uint d=block_size; d>0; d>>=1) {
        if(lid < d) {
            ai = offset*(lofst) -1;
            bi = offset*(lofst+1) -1;

            #ifdef ZERO_BANK_CONFLICTS
            ai += CONFLICT_FREE_OFFSET(ai);
            bi += CONFLICT_FREE_OFFSET(bi);
            #endif

            input[bi] += input[ai];
        }
        offset <<= 1;
        barrier(CLK_LOCAL_MEM_FENCE);
    }

    // clear the last element
    if(lid == 0) { // bi currently points at (2*block_size -1)
        partSums[groupid] = input[bi];
        input[bi] = 0;
    }
    
}


inline void downsweep(__local uint* input)
{
    uint lid = get_local_id(0);
    uint double_block = get_local_size(0)<<1;
    uint lofst = (lid<<1)+1;
    uint ai, bi, t, offset;

    offset = double_block;
    for(uint d=1; d<double_block; d<<=1) {
        offset >>= 1;
        barrier(CLK_LOCAL_MEM_FENCE);
        if(lid < d) {
            ai = offset*(lofst) -1;
            bi = offset*(lofst+1) -1;

            #ifdef ZERO_BANK_CONFLICTS
            ai += CONFLICT_FREE_OFFSET(ai);
            bi += CONFLICT_FREE_OFFSET(bi);
            #endif

            t = input[ai];
            input[ai] = input[bi];
            input[bi] += t;
        }
    }
    barrier(CLK_LOCAL_MEM_FENCE);
}
