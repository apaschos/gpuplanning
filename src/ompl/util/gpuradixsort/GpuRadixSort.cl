/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */


/*
      - Performs count, upsweep, downsweep and (optionally) group reorder
*/
__kernel void localSort(__global const uint* input,
            __global uint* partialSums,
            __global uint* localPermute,
            __local uint* localHistos,
            const uint lineLength,
            const uint step,
            const uint pass,
            const uint globalreorder);

/*
      - Calculates the permute array for the specific pass
*/
__kernel void globalReorder(__global const uint* input,
            __global uint* globalPermut,
            __global uint* scannedPartSums,
            const uint blockStep,
            const uint lineLength,
            const uint pass);

__kernel void sumPerms(__global const uint* localPerm,
            __global uint* globalPerm,
            const uint globalreorder);

/*
      - Based on the permute array, places the input data at the right place of the output array (permutation of input)
*/
__kernel void computeOutput(__global const uint* input,
            __global uint* output,
            __global const uint* glob_permut,
            const uint lineLength);

/*
      - Both kernels calculate the totalPermutation up until this point
*/
__kernel void computeTotalPerm1(__global uint* tempPerm,
            __global const uint* TotalPermuts);

__kernel void computeTotalPerm2(__global const uint* permut,
            __global uint* TotalPermuts,
            __global const uint* tempPerm);

/*
      - Calculates the inverse permutation array based on the permutation array
*/
__kernel void computeInversePermute(__global const uint* permut,
            __global uint* invPermut);

/* Upsweep phase of prefix sum */
inline void upsweep(__local uint* input,
            __global uint* partSums,
            const uint k);

/* Downsweep phase of prefix sum */
inline void downsweep(__local uint* input,
            const uint k);

/* Calculates the histogram for each thread */
inline void count(__private const uint* bkeys,
            __local uint* histos);

/* Reorders the elements inside the work group */
inline void local_reorder(uint* bkeys,
            __global uint* permut,
            __global uint* partSums,
            __local uint* histos,
            const uint step,
            const uint globalreorder);


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////


__kernel void localSort(__global const uint* input,
            __global uint* partialSums,
            __global uint* localPermute,
            __local uint* localHistos,
            const uint lineLength,
            const uint step,
            const uint pass,
            const uint globalreorder)
{
    uint gid = get_global_id(0);
    uint chunkofs = gid * CHUNK;
    uint keys[CHUNK];
    uint val;

    // Fetch the data from the global memory
    for(uint i=0; i<CHUNK; i++) {
        val = input[(chunkofs + i) * lineLength];
        keys[i] = (val >> (pass*BITS)) & (BUCKETS-1);
    }

    count(keys, localHistos);

    for(uint k=0; k<BUCKETS; k+=2) {
        upsweep(localHistos, partialSums, k);
        downsweep(localHistos, k);
    }

    local_reorder(keys, localPermute, partialSums, localHistos, step, globalreorder);
}


__kernel void globalReorder(__global const uint* input,
            __global uint* globalPermut,
            __global uint* scannedPartSums,
            const uint blockStep,
            const uint lineLength,
            const uint pass)
{
    uint gid = get_global_id(0);
    uint lid = get_local_id(0);
    uint groupid = get_group_id(0);
    uint total_groups = get_num_groups(0);
    uint block_size = get_local_size(0);
    uint bstart = groupid/blockStep*blockStep;
    uint bend = bstart + blockStep;
	uint group_offset = bstart*CHUNK*block_size;
    uint chunkofs = gid * CHUNK;
    uint key, val, index, bofs, global_offset;

	for(uint i=0; i<CHUNK; i++) {
        index = chunkofs + i;
        val = input[index * lineLength];
        key = (val >> (pass*BITS)) & (BUCKETS-1);

        global_offset = 0;
        // Get number of elements in all buckets with key less than the current
        for(uint l=0; l<key; l++) {
            bofs = l * total_groups;
            global_offset += (scannedPartSums[bofs + bend] - scannedPartSums[bofs + bstart]);
        }

        // Get number of elements in previous buckets with the same key
        bofs = key * total_groups;
        global_offset += (scannedPartSums[bofs + groupid] - scannedPartSums[bofs + bstart]);

        // Store offset to permute array
        globalPermut[index] = global_offset + group_offset;
    }
}


__kernel void sumPerms(__global const uint* localPerm,
            __global uint* globalPerm,
            const uint globalreorder)
{
    uint gid = get_global_id(0);
    uint perm;

    if(globalreorder) {
        perm = globalPerm[gid] + localPerm[gid];
    }
    else {
        perm = localPerm[gid] + (get_group_id(0)/CHUNK * CHUNK) * get_local_size(0);
    }

    globalPerm[gid] = perm;
}


__kernel void computeOutput(__global const uint* input,
            __global uint* output,
            __global const uint* glob_permut,
            const uint lineLength)
{
    uint gid = get_global_id(0);
    uint inindx, outindx;

    inindx = gid * lineLength;
    outindx = glob_permut[gid] * lineLength;

    for(uint i=0; i<lineLength; i++) {
        output[outindx+i] = input[inindx+i];
    }
}


__kernel void computeTotalPerm1(__global uint* tempPerm,
            __global const uint* TotalPermuts)
{
    uint gid = get_global_id(0);

    tempPerm[gid] = TotalPermuts[gid];
}


__kernel void computeTotalPerm2(__global const uint* permut,
            __global uint* TotalPermuts,
            __global const uint* tempPerm)
{
    uint gid = get_global_id(0);

    TotalPermuts[gid] = permut[tempPerm[gid]];
}


__kernel void computeInversePermute(__global const uint* permut,
            __global uint* invPermut)
{
    uint gid = get_global_id(0);

    invPermut[permut[gid]] = gid;
}




inline void count(__private const uint* bkeys,
            __local uint* histos)
{
    uint lid = get_local_id(0);
    uint block_size = get_local_size(0);
    uint ct;

    // Initialize the counters for each thread
    // #pragma unroll
    for(uint i=0; i<BUCKETS; i++) {
        histos[i*block_size+lid] = 0;
    }

    // Fetch the data and increase the corresponding counter
    for(uint i=0; i<CHUNK; i++) {
        ct = bkeys[i] * block_size + lid;
        histos[ct]++;
    }

    barrier(CLK_LOCAL_MEM_FENCE);
}


inline void upsweep(__local uint* input,
            __global uint* partSums,
            const uint k)
{
    uint lid = get_local_id(0);
    uint groupid = get_group_id(0);
    uint block_size = get_local_size(0);
    uint total_groups = get_num_groups(0);
    uint half_block = (block_size>>1);
    uint ai, bi, lofst, bucket_offset, offset;
    bool first_half;

    // Multiscan: half threads work for one bucket and the other half for the next bucket
    if(lid<half_block) {
        bucket_offset = k * block_size;
        first_half = true;
    }
    else {
        bucket_offset = (k+1) * block_size;
        lid = lid - half_block;
        first_half = false;
    }
    lofst = lid<<1;

    offset = 1;
    for(uint d=half_block; d>0; d>>=1) {
        if(lid < d) {
            ai = offset*(lofst+1) -1 + bucket_offset;
            bi = offset*(lofst+2) -1 + bucket_offset;

            input[bi] += input[ai];
        }
        offset <<= 1;
        barrier(CLK_LOCAL_MEM_FENCE);
    }

    // Clear the last element
    if(lid == 0) {
        offset = bucket_offset + block_size-1;
        if(first_half) {
            partSums[k * total_groups + groupid] = input[offset];
        }
        else {
            partSums[(k+1) * total_groups + groupid] = input[offset];
        }
        input[offset] = 0;
    }
}


inline void downsweep(__local uint* input,
            const uint k)
{
    uint lid = get_local_id(0);
    uint block_size = get_local_size(0);
    uint half_block = (block_size>>1);
    uint ai, bi, t, lofst, offset, bucket_offset;

    // Multiscan: half threads work for one bucket and the other half for the next bucket
    if(lid<half_block) {
        bucket_offset = k * block_size;
    }
    else {
        bucket_offset = (k+1) * block_size;
        lid = lid - half_block;
    }
    lofst = lid<<1;

    offset = block_size;
    for(uint d=1; d<block_size; d<<=1) {
        offset >>= 1;
        barrier(CLK_LOCAL_MEM_FENCE);
        if(lid < d) {
            ai = offset*(lofst+1) -1 + bucket_offset;
            bi = offset*(lofst+2) -1 + bucket_offset;

            t = input[ai];
            input[ai] = input[bi];
            input[bi] += t;
        }
    }

    barrier(CLK_LOCAL_MEM_FENCE);
}


inline void local_reorder(uint* bkeys,
            __global uint* permut,
            __global uint* partSums,
            __local uint* histos,
            const uint step,
            const uint globalreorder)
{
    uint lid = get_local_id(0);
    uint groupid = get_group_id(0);
    uint total_groups = get_num_groups(0);
    uint block_size = get_local_size(0);
    uint start = lid/step*step;
    uint end = start + step;
    uint startofs = start * CHUNK;
    uint chunkofs = get_global_id(0) * CHUNK;
    uint PSums[BUCKETS];
    uint inchunk_ofs[BUCKETS]; // used because CHUNK may contain two keys with same values
    uint buck_sms[BUCKETS];
    uint total, key, local_offset;


    barrier(CLK_GLOBAL_MEM_FENCE); // for partSums from upsweep()

    // copy partSums of group from global to private memory (we could use local memory but it is filled from histos)
    // #pragma unroll
    for(uint i=0; i<BUCKETS; i++) {
        PSums[i] = partSums[i * total_groups + groupid];
    }

    // initialize inchunk_ofs to 0
    // #pragma unroll
    for(uint i=0; i<BUCKETS; i++) {
        inchunk_ofs[i] = 0;
    }

    // compute the desired bucket sums a priori (in prefix sum way)
    if(!globalreorder) {
        total = 0;
        // #pragma unroll
        for(uint l=0; l<BUCKETS; l++) {
            buck_sms[l] = total;
            if(end == block_size) {
                total += PSums[l];
            }
            else {
                total += histos[l * block_size + end];
            }
            total -= histos[l * block_size + start];
        }
    }

    // compute offset inside work group
    for(uint i=0; i<CHUNK; i++) {
        key = bkeys[i];
        local_offset = 0;

        // Get number of elements in all work items with key less than the current
        if(!globalreorder) { // this step is also performed more efficiently in global_reorder
            local_offset += buck_sms[key];
        }
        
        // Get number of elements in previous work items with the same key
        local_offset += (histos[key * block_size + lid] - histos[key * block_size + start]);

        local_offset += inchunk_ofs[key]++;


        permut[chunkofs + i] = local_offset + startofs;
    }
}
