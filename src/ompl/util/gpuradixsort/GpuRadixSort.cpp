/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */

#include "ompl/util/gpuradixsort/GpuRadixSort.h"
#include <cassert>


namespace ompl{

    typedef enum {
        LocalSort,
	    GlobalReorder,
        SumPerms,
	    ComputeOutput,
	    ComputeTotalPerm1,
	    ComputeTotalPerm2,
	    ComputeInvPerm
    } kernelFunctions;


    GpuRadixSort::GpuRadixSort(const cl_uint chunk,
    	const cl_uint bitsperpass, const cl_uint totalbits) : OpenCL()
    {
        setTotalBits(totalbits);
        setBitsPerPass(bitsperpass);
        setChunkSize(chunk);
        setBuckets(1 << bits);
        assert(((buckets * max_work_group_size) <= local_mem_size)
                && "Local Memory requirements exceed the device's maximum.\nSet a lower number in bitsperpass");

    	setup();
    }


    void GpuRadixSort::setup(void)
    {
        programSources = { loadSource("../src/ompl/util/gpuradixsort/GpuRadixSort.cl") };

        std::string params = "";
        params += std::string("-D TOTALBITS=") + std::to_string(totalbits);
        params += std::string(" -D BITS=") + std::to_string(bits);
        params += std::string(" -D BUCKETS=") + std::to_string(buckets);
        params += std::string(" -D CHUNK=") + std::to_string(chunk);
        programParams = { params };

        kernelNames = { {"localSort", "globalReorder", "sumPerms", "computeOutput",
                "computeTotalPerm1", "computeTotalPerm2", "computeInversePermute"} };

        OpenCL::setup();
    }


    void GpuRadixSort::computeInversePermutation(cl::Buffer& d_Perm, cl::Buffer& d_InvPerm)
    {
        cl_uint rows = getBufferSize<cl_uint>(d_Perm);

        setCurrentKernel(ComputeInvPerm);
        setGlobalArg(d_Perm);
        setGlobalArg(d_InvPerm);

        runKernel(rows, max_work_group_size);
    }


    void GpuRadixSort::setChunkSize(cl_uint ch)
    {
        chunk = ch;
    }


    void GpuRadixSort::setBitsPerPass(cl_uint b)
    {
        bits = b;
    }


    void GpuRadixSort::setTotalBits(cl_uint b)
    {
        this->totalbits = b;
    }


    void GpuRadixSort::setBuckets(cl_uint bucks)
    {
        buckets = bucks;
    }


    cl_uint GpuRadixSort::getChunkSize()
    {
        return chunk;
    }


    cl_uint GpuRadixSort::getBitsPerPass()
    {
        return bits;
    }


    cl_uint GpuRadixSort::getTotalBits()
    {
        return totalbits;
    }


    cl_uint GpuRadixSort::getBuckets()
    {
        return buckets;
    }


    void GpuRadixSort::sort(cl::Buffer& d_Input)
    {
        cl_uint rows = getBufferSize<cl_uint>(d_Input);

        sort(d_Input, NULL, NULL, 0, rows, 1, rows);
    }


    void GpuRadixSort::sort(cl::Buffer& d_Input, cl::Buffer& d_Output)
    {
        cl_uint rows = getBufferSize<cl_uint>(d_Input);

        sort(d_Input, &d_Output, NULL, 0, rows, 1, rows);

    }


    void GpuRadixSort::sortByKeys(cl::Buffer& d_Input, const cl_uint columns)
    {
        cl_uint rows = getBufferSize<cl_uint>(d_Input) / columns;

        sort(d_Input, NULL, NULL, 0, rows, columns, rows);
    }


    void GpuRadixSort::sortByKeys(cl::Buffer& d_Input, cl::Buffer& d_Output,
            const cl_uint columns)
    {
        cl_uint rows = getBufferSize<cl_uint>(d_Input) / columns;

        sort(d_Input, &d_Output, NULL, 0, rows, columns, rows);
    }


    void GpuRadixSort::multipleSort(cl::Buffer& d_Input, const cl_uint multiple)
    {
        cl_uint rows = getBufferSize<cl_uint>(d_Input);

        sort(d_Input, NULL, NULL, 0, rows, 1, multiple);
    }


    void GpuRadixSort::multipleSort(cl::Buffer& d_Input, cl::Buffer& d_Output, const cl_uint multiple)
    {
        cl_uint rows = getBufferSize<cl_uint>(d_Input);

        sort(d_Input, &d_Output, NULL, 0, rows, 1, multiple);
    }


    void GpuRadixSort::multipleSortByKeys(cl::Buffer& d_Input, const cl_uint columns,
                const cl_uint multiple)
    {
        cl_uint rows = getBufferSize<cl_uint>(d_Input) / columns;

        sort(d_Input, NULL, NULL, 0, rows, columns, multiple);
    }


    void GpuRadixSort::multipleSortByKeys(cl::Buffer& d_Input, cl::Buffer& d_Output,
                const cl_uint columns, const cl_uint multiple)
    {
        cl_uint rows = getBufferSize<cl_uint>(d_Input) / columns;

        sort(d_Input, &d_Output, NULL, 0, rows, columns, multiple);
    }


    void GpuRadixSort::sortPermute(cl::Buffer& d_Input, cl::Buffer& d_Perm)
    {
        cl_uint rows = getBufferSize<cl_uint>(d_Input);

        sort(d_Input, NULL, &d_Perm, 0, rows, 1, rows);
    }


    void GpuRadixSort::sortPermute(cl::Buffer& d_Input, cl::Buffer& d_Output, cl::Buffer& d_Perm)
    {
        cl_uint rows = getBufferSize<cl_uint>(d_Input);

        sort(d_Input, &d_Output, &d_Perm, 0, rows, 1, rows);
    }


    void GpuRadixSort::sortPermuteByKeys(cl::Buffer& d_Input, cl::Buffer& d_Perm,
                const cl_uint columns)
    {
        cl_uint rows = getBufferSize<cl_uint>(d_Input) / columns;

        sort(d_Input, NULL, &d_Perm, 0, rows, columns, rows);
    }


    void GpuRadixSort::sortPermuteByKeys(cl::Buffer& d_Input, cl::Buffer& d_Output,
                cl::Buffer& d_Perm, const cl_uint columns)
    {
        cl_uint rows = getBufferSize<cl_uint>(d_Input) / columns;

        sort(d_Input, &d_Output, &d_Perm, 0, rows, columns, rows);
    }


    /*
        -Sorts the input (based on radix sort)
        -Computes the permutation array for converting the input to output
        -In case of d_Output == NULL, d_Input is used as output
        -In case of d_Perm == NULL, totalpermutation kernels are not executed, so the permutation array from input to output is not kept
        -In case of size of input buffer not a multiple of max_work_group_size, output is extended to be so (except if d_Input is output(cannot extend an allocated buffer))
        -If d_Output or d_Perm are not NULL, they should be deleted outside of this function (after it is executed)
    */
    void GpuRadixSort::sort(cl::Buffer& d_Input, cl::Buffer* d_Output, cl::Buffer* d_Perm,
                        const cl_uint startIndex, const cl_uint rows, const cl_uint columns,
                        const cl_uint multiple)
    {
        cl_uint passes; // number of passes of radix sort
        cl_uint extended_rows;
        cl_uint initialsize;
        cl_uint finalsize;
        cl_uint differ;
        cl_uint groupchunk;
        cl_uint blockStep; // holds how many work-groups multiple covers
        cl_uint threadStep; // holds how many work-items (in the range of a work-group) multiple covers
        cl_uint multipleBlocks; // will be used for the size of d_PartialSums buffer
        cl_uint sumElems;
        bool output = false;
        bool totalperm = false;
        bool glob_reord = false;

        cl::Buffer d_TempOut;
        cl::Buffer d_GlobalPermute;
        cl::Buffer d_LocalPermute;
        cl::Buffer d_passPerm;
        cl::Buffer d_PartialSums;
        cl::Buffer d_PrefixSum_PartialSums;
        cl::Buffer *d_NewOutput;
        cl::Buffer d_OutputAlias;


        assert(multiple && "Multiple should be bigger than 0.");
        if(multiple < chunk) {
            chunk = multiple;
            setup();
        }
        passes = totalbits/bits + (totalbits%bits?1:0);
        groupchunk = chunk * max_work_group_size;

        // extend number of threads to be a multiple of groupchunk
        extended_rows = extendToMultiple(rows, groupchunk);
        assert(!(extended_rows % multiple) && "Multiple should be a multiple of total elements");
        initialsize = rows * columns;
        finalsize = extended_rows * columns;
        differ = finalsize - initialsize;
        blockStep = multiple / groupchunk;
        if(!blockStep) {
           assert(!(max_work_group_size % multiple) &&
                "Multiple should be a multiple of max_work_group_size");
        }
        threadStep = (blockStep? max_work_group_size : multiple) / chunk;

        multipleBlocks = (buckets * extended_rows) / groupchunk;


        // If there is no output buffer, result is stored in input
        if(d_Output == nullptr) {
            assert(!differ && "Input buffer must have size multiple of (max_work_group_size*CHUNK) in case of no output buffer");
        
            // If the whole input buffer will be sorted, we just point output to input
            if(initialsize==getBufferSize<cl_uint>(d_Input)) {
                d_NewOutput = &d_Input;
            }
            // Else we create a subbuffer that holds only the required data and point output to it
            else {
                d_OutputAlias = SubBuffer<cl_uint>(d_Input, startIndex * columns, initialsize);
                d_NewOutput = &d_OutputAlias; // no copyBetweenBuffers(), input buffer data is not backed up
            }
        }
        // Otherwise, we copy the input buffer to another one in order to preserve the original data
        else {
            output = true;
            d_NewOutput = CLBufferPtr<cl_uint>(CL_MEM_READ_WRITE, finalsize);
            copyBetweenBuffers<cl_uint>(d_Input, *d_NewOutput, startIndex * columns, differ, initialsize);

            // If size of input buffer is not a multiple of max_work_group_size, we extend it with zeros 
            if(differ) {
                // Fill the extra elements with zeros
                cl_uint* zeros = new cl_uint[differ];
                for(cl_uint i=0; i<differ; i+=columns) // we don't care for the rest of the values
                {
                    zeros[i] = 0;
                }
                // The zeros elements are placed in the beginning to minimize calculations and permutations
                #if(__OPENCL_VERSION__ >= 120)
                cl::Buffer d_dif = CLBuffer<cl_uint>(CL_MEM_HOST_WRITE_ONLY, differ);
                writeToBuffer(d_dif, 0, differ, zeros);
                copyBetweenBuffers<cl_uint>(d_dif, *d_NewOutput, 0, 0, differ);
                #else
                writeToBuffer(*d_NewOutput, 0, differ, zeros);
                #endif
                delete[] zeros;
            }
        }

        d_TempOut = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, finalsize);
        d_GlobalPermute = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, extended_rows);
        d_LocalPermute = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, extended_rows);
        d_PartialSums = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, multipleBlocks);


        if(d_Perm != nullptr) {
            d_passPerm = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, extended_rows);
            totalperm = true;

            in.initializeBufferLinear(*d_Perm);
        }

        // if global reorder is required
        if((multipleBlocks > 1) && (blockStep > 0)) {
            glob_reord = true;
            
            d_PrefixSum_PartialSums = CLBuffer<cl_uint>(CL_MEM_READ_WRITE, (multipleBlocks + 1));
        }

        // Setup of kernel arguments
        setCurrentKernel(LocalSort);
        setGlobalArg(*d_NewOutput);
        setGlobalArg(d_PartialSums);
        setGlobalArg(d_LocalPermute);
        setLocalArg(buckets * max_work_group_size * sizeof(cl_uint));
        setConstantArg(columns);
        setConstantArg(threadStep);
        setConstantArg(static_cast<cl_uint>(glob_reord), 7);

        setCurrentKernel(SumPerms);
        setGlobalArg(d_LocalPermute);
        setGlobalArg(d_GlobalPermute);
        setConstantArg(static_cast<cl_uint>(glob_reord));

        setCurrentKernel(ComputeOutput);
        setGlobalArg(*d_NewOutput);
        setGlobalArg(d_TempOut);
        setGlobalArg(d_GlobalPermute);
        setConstantArg(columns);

        // If total permutation is required
        if(totalperm) {
            setCurrentKernel(ComputeTotalPerm1);
            setGlobalArg(d_passPerm);
            setGlobalArg(*d_Perm);

            setCurrentKernel(ComputeTotalPerm2);
            setGlobalArg(d_GlobalPermute);
            setGlobalArg(*d_Perm);
            setGlobalArg(d_passPerm);       
        }

        if(glob_reord) {
            setCurrentKernel(GlobalReorder);
            setGlobalArg(*d_NewOutput);
            setGlobalArg(d_GlobalPermute);
            setGlobalArg(d_PrefixSum_PartialSums);
            setConstantArg(blockStep);
            setConstantArg(columns);
        }

        // Needed to switch input and output at each pass
        cl::Buffer* b1;
        cl::Buffer* b2;
        // At each pass perform count, scan and reorder
        // Then, calculate the output array (input for the next pass) and the new total permutation array
        for(cl_uint pass=0; pass<passes; pass++) {

            // switch input output
            if(pass%2) {
                b2 = d_NewOutput;
                b1 = &d_TempOut;
            }
            else {
                b1 = d_NewOutput;
                b2 = &d_TempOut;
            }

            setCurrentKernel(LocalSort);
            setGlobalArg(*b1);
            setConstantArg(pass, 6);
            runKernel(extended_rows/chunk, max_work_group_size);

            if(glob_reord)
            {
                sc.scan(d_PartialSums, d_PrefixSum_PartialSums);
                sumElems = sc.getTotal()-1;
                writeToBuffer(d_PrefixSum_PartialSums, multipleBlocks, 1, &sumElems);

    		    setCurrentKernel(GlobalReorder);
    		    setGlobalArg(*b1);
    		    setConstantArg(pass, 5);
    		    runKernel(extended_rows/chunk, max_work_group_size);

            }

            // Sum up permutation arrays (global and local)
            setCurrentKernel(SumPerms);
            runKernel(extended_rows, max_work_group_size);

    	    setCurrentKernel(ComputeOutput);
    	    setGlobalArg(*b1);
    	    setGlobalArg(*b2);
    	    runKernel(extended_rows, max_work_group_size);

            if(totalperm) {
                setCurrentKernel(ComputeTotalPerm1);
                runKernel(extended_rows, max_work_group_size);

                setCurrentKernel(ComputeTotalPerm2);
                runKernel(extended_rows, max_work_group_size);
            }
        } // end of passes

        if(passes%2) {
            if(output) {
                copyBetweenBuffers<cl_uint>(d_TempOut, *d_Output, differ, 0, initialsize);
                delete d_NewOutput;
            }
            else {
                d_Input = d_TempOut;
            }
        }
        else {
            // Result is already stored in d_Input

            if(output) {
                copyBetweenBuffers<cl_uint>(*d_NewOutput, *d_Output, differ, 0, initialsize);
                delete d_NewOutput;
            }
        }
    }

}