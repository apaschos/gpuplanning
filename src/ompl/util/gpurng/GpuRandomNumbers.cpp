/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */

#include "ompl/util/gpurng/GpuRandomNumbers.h"
#include <chrono>
#include <cassert>


namespace ompl{

    typedef enum {
        RandGen
    } kernelFunctions;


    GpuRNG::GpuRNG(cl_uint dim1, cl_uint dim2, cl_uint generator) : OpenCL()
    {
        this->dim1 = dim1;
        this->dim2 = dim2;
        boundsSet = false;
        seedSet = false;
        
        setSeed(generator);
        setup();

        // if(dim1*dim2<1) {
        //     OMPL_ERROR("Size should be a number greater than 0.");
        // }
        assert(dim1*dim2>=1 && "Size should be a number greater than 0");

        d_lower_bounds = CLBuffer<cl_int>(CL_MEM_READ_ONLY, dim2);
        d_range = CLBuffer<cl_int>(CL_MEM_READ_ONLY, dim2);
    }


    void GpuRNG::setup(void)
    {
        programSources = { loadSource("../src/ompl/util/gpurng/GpuRandomNumbers.cl") };
        programParams = { (std::string("-D DIMS=") + std::to_string(dim2)) };
        kernelNames = { {"randGen"} };

        OpenCL::setup();
    }


    cl_uint GpuRNG::getSeed()
    {
        return generator;
    }


    void GpuRNG::setSeed(cl_uint seed)
    {
        if(seed == 0) {
            generator = std::chrono::duration_cast<std::chrono::microseconds>
                    (std::chrono::system_clock::now().time_since_epoch()).count();
        }
        else {
            generator = seed;
        }

        seedSet = true;
    }


    void GpuRNG::setBounds(std::vector<cl_int> lower_bounds, std::vector<cl_int> upper_bounds)
    {
        // if(lower_bounds.empty() || upper_bounds.empty()) {
            // OMPL_ERROR("Bounds are not set.");
        // }
        assert((!lower_bounds.empty() && !upper_bounds.empty()) && "Bounds are not set.");

        // if((lower_bounds.size() != dim2) || (upper_bounds.size() != dim2)) {
            // OMPL_ERROR("Lower and Upper Bounds' vectors have wrong size.");
        // }
        assert((lower_bounds.size() == dim2) && (upper_bounds.size() == dim2) &&
                    "Lower and Upper Bounds' vectors have wrong size.");

        
        cl_int* lowerBounds = new cl_int[dim2];
        cl_int* upperBounds = new cl_int[dim2];

        std::copy(lower_bounds.begin(), lower_bounds.end(), lowerBounds);
        std::copy(upper_bounds.begin(), upper_bounds.end(), upperBounds);

        setBounds(lowerBounds, upperBounds);

        if(dim2>1) {
            delete[] lowerBounds;
            delete[] upperBounds;
        }
        else {
            delete lowerBounds;
            delete upperBounds;
        }
    }


    void GpuRNG::setBounds(cl_int lower_bound, cl_int upper_bound) {
        // if(lower_bound >= upper_bound) {
        //     OMPL_ERROR("Lower bound exceeds upper_bound.");
        // }
        assert((lower_bound < upper_bound) && "Lower bound exceeds upper_bound.");

        cl_int* lowerBounds = new cl_int[dim2];
        cl_int* upperBounds = new cl_int[dim2];

        for(uint i=0; i<dim2; i++) {
            lowerBounds[i] = lower_bound;
            upperBounds[i] = upper_bound;
        }

        setBounds(lowerBounds, upperBounds);

        delete[] lowerBounds;
        delete[] upperBounds;

    }


    void GpuRNG::setBounds(cl_int* lower_bounds, cl_int* upper_bounds) {
        cl_int* range = new cl_int[dim2];

        for(uint i=0; i<dim2; i++) {
            range[i] = upper_bounds[i] - lower_bounds[i];
        }

        writeToBuffer(d_lower_bounds, 0, dim2, lower_bounds);
        writeToBuffer(d_range, 0, dim2, range);

        boundsSet = true;

        delete[] range;
    }


    void GpuRNG::generateRandomData(cl::Buffer& data)
    {
        // if(!boundsSet)
        // {
        //     OMPL_ERROR("Random number generator has no bounds set.");
        // }
        // if(!seedSet)
        // {
        //     OMPL_ERROR("Random number generator has no seed set.");
        // }
        assert(boundsSet && "Random number generator has no bounds set.");
        assert(seedSet && "Random number generator has no seed set.");

        data = CLBuffer<cl_int>(CL_MEM_GPU_ONLY, (dim1*dim2) );

        setCurrentKernel(RandGen);
        
        setGlobalArg(data);
        setGlobalArg(d_lower_bounds);
        setGlobalArg(d_range);
        setConstantArg(generator);

        runKernel(dim1, max_work_group_size);
    }




    RNG::RNG(cl_uint generator)
    {
        setSeed(generator);
    }


    cl_uint RNG::getSeed()
    {
        return generator;
    }

    void RNG::setSeed(cl_uint seed)
    {
        if(seed == 0) {
            generator = std::chrono::duration_cast<std::chrono::milliseconds>
                (std::chrono::system_clock::now().time_since_epoch()).count();
        }
        else {
            generator = seed;
        }

         rng.seed(generator);
    }
}