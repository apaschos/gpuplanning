/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */

#include "ompl/util/gpubufinit/GpuBufInit.h"


namespace ompl{

    typedef enum {
        Init,
        Init64,
        InitKeys,
        initLinear,
        initKeysLinear,
    } kernelFunctions;


	GpuBufInit::GpuBufInit() : OpenCL()
	{
	    setup();
	}
	
	
	void GpuBufInit::setup(void)
	{
	    programSources = { loadSource("../src/ompl/util/gpubufinit/GpuBufInit.cl") };
	    // programParams = { "" };
	    kernelNames = { {"init", "init64", "initKeys", "initLinear", "initKeysLinear"} };
	
	    OpenCL::setup();
	}
	
	
	void GpuBufInit::initializeBuffer(cl::Buffer& d_data, cl_uint value,
	                cl_int start, cl_int end)
	{
	    cl_uint rows = getBufferSize<cl_uint>(d_data);
	    if(start == -1) {
	        start = 0;
	        end = rows;
	    }
	    else {
	        rows = end-start;
	    }
	
	    setCurrentKernel(Init);
	    
	    setGlobalArg(d_data);
	    setConstantArg(value);
	    setConstantArg(start);
	    setConstantArg(end);
	
	    runKernel(rows);
	}
	
	
	void GpuBufInit::initializeBuffer64bit(cl::Buffer& d_data, cl_uint value1, cl_uint value2,
	                cl_int start, cl_int end)
	{
	    cl_uint rows = getBufferSize<cl_uint>(d_data) / 2;
	    if(start == -1) {
	        start = 0;
	        end = rows;
	    }
	    else {
	        rows = end-start;
	    }
	
	    setCurrentKernel(Init64);
	    
	    setGlobalArg(d_data);
	    setConstantArg(value1);
	    setConstantArg(value2);
	    setConstantArg(start);
	    setConstantArg(end);
	
	    runKernel(rows);
	}
	
	
	void GpuBufInit::initializeBuffer_KeysOnly(cl::Buffer& d_data, cl_uint value, cl_uint columns,
	                cl_int start, cl_int end)
	{
	    cl_uint rows = getBufferSize<cl_uint>(d_data) / columns;
	    if(start == -1) {
	        start = 0;
	        end = rows;
	    }
	    else {
	        rows = end-start;
	    }
	
	    setCurrentKernel(InitKeys);
	    
	    setGlobalArg(d_data);
	    setConstantArg(value);
	    setConstantArg(columns);
	    setConstantArg(start);
	    setConstantArg(end);
	
	    runKernel(rows);
	}
	
	
	void GpuBufInit::initializeBufferLinear(cl::Buffer& d_data, cl_int start, cl_int end)
	{
	    cl_uint rows = getBufferSize<cl_uint>(d_data);
	    if(start == -1) {
	        start = 0;
	        end = rows;
	    }
	    else {
	        rows = end-start;
	    }
	
	    setCurrentKernel(initLinear);
	    
	    setGlobalArg(d_data);
	    setConstantArg(start);
	    setConstantArg(end);
	
	    runKernel(rows);
	}
	
	
	void GpuBufInit::initializeBufferLinear_KeysOnly(cl::Buffer& d_data, cl_uint columns,
	                cl_int start, cl_int end)
	{
	    cl_uint rows = getBufferSize<cl_uint>(d_data) / columns;
	    if(start == -1) {
	        start = 0;
	        end = rows;
	    }
	    else {
	        rows = end-start;
	    }
	
	    setCurrentKernel(initKeysLinear);
	    
	    setGlobalArg(d_data);
	    setConstantArg(columns);
	    setConstantArg(start);
	    setConstantArg(end);
	
	    runKernel(rows);
	}

}
