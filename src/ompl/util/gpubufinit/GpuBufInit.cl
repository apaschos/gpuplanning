/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */


__kernel void init(__global uint* A,
                const uint value,
                const uint start,
                const uint end)
{
	uint gid = get_global_id(0);
	uint offset = gid + start;

	if(offset < end) {
		A[offset] = value;
	}
}


__kernel void init64(__global uint2* A,
				const uint value1,
				const uint value2,
                const uint start,
                const uint end)
{
	uint gid = get_global_id(0);
	uint offset = gid + start;

	if(offset < end) {
		A[offset] = (uint2) (value2, value1); // inverted because of endianness
	}
}


__kernel void initKeys(__global uint* A,
				const uint value,
				const uint columns,
				const uint start,
                const uint end)
{
	uint gid = get_global_id(0);
	uint offset = gid + start;

	if(offset < end) {
		A[offset*columns] = value;
	}
}


__kernel void initLinear(__global uint* A,
				const uint start,
                const uint end)
{
	uint gid = get_global_id(0);
	uint offset = gid + start;

	if(offset < end) {
		A[offset] = gid;
	}
}


__kernel void initKeysLinear(__global uint* A,
				const uint columns,
				const uint start,
                const uint end)
{
	uint gid = get_global_id(0);
	uint offset = gid + start;

	if(offset < end) {
		A[offset*columns] = gid;
	}
}
