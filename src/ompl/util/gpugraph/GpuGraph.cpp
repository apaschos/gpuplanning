/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */

#include "ompl/util/gpugraph/GpuGraph.h"


namespace ompl{

	GpuGraph::GpuGraph(const cl_uint edges) : OpenCL()
	{
		this->edges = edges;
	}


	void GpuGraph::setup(void)
	{
		programSources.push_back( loadSource("../src/ompl/util/gpugraph/GpuGraph.cl") );
		kernelNames.push_back( { "getPath" } );

		OpenCL::setup();
	}


	void GpuGraph::getPath(cl::Buffer& d_Path, const cl_uint Vstart, const cl_uint Vgoal)
	{
		cl::Buffer d_pth = CLBuffer<cl_uint>(CL_MEM_READ_WRITE, nodes);
		cl::Buffer d_Hops = CLBuffer<cl_uint>(CL_MEM_READ_WRITE, 1);
		cl_uint hops;


		setCurrentKernel(getNumKernels()-1);

		setGlobalArg(d_P);
		setGlobalArg(d_pth);
		setGlobalArg(d_Hops);
		setConstantArg(Vstart);
		setConstantArg(Vgoal);

		runKernel(1, 1);

		readFromBuffer(d_Hops, 0, 1, &hops);
		d_Path = CLBuffer<cl_uint>(CL_MEM_READ_WRITE, hops);
		copyBetweenBuffers<cl_uint>(d_pth, d_Path, 0, 0, hops);
	}

}
