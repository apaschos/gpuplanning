/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */

#include "ompl/util/gpugraph/gpubfs/GpuBFS.h"


namespace ompl{

    typedef enum {
    	BFS
    } kernelFunctions;


	GpuBFS::GpuBFS(const cl_uint edges) : GpuGraph(edges)
	{
		setup();
	}


	void GpuBFS::setup(void)
	{
	    programSources = { loadSource("../src/ompl/util/gpugraph/gpubfs/GpuBFS.cl") };
	    // programParams = { "" };
	    kernelNames = { {"bfs"} };

	    GpuGraph::setup();
	}


	int GpuBFS::bfs(cl::Buffer& d_Graph, const cl_uint Vstart, const cl_uint Vgoal)
	{
		cl_uint alarm[2];
		cl_uint level;
		nodes = getBufferSize<node>(d_Graph) / edges;

		d_F = CLBuffer<cl_uint>(CL_MEM_READ_WRITE, nodes);
		d_P = CLBuffer<cl_uint>(CL_MEM_READ_WRITE, nodes); // parent id
		d_alarm = CLBuffer<cl_uint>(CL_MEM_READ_WRITE, 2); // alarm that graph is fully searched or goal is reached


		in.initializeBuffer(d_F, CL_UINT_MAX);
		level = 0;
		writeToBuffer(d_F, Vstart, 1, &level);

		setCurrentKernel(BFS);
		setGlobalArg(d_Graph);
		setGlobalArg(d_F);
		setGlobalArg(d_P);
		setGlobalArg(d_alarm);
		setConstantArg(level);
		setConstantArg(edges);
		setConstantArg(Vgoal);

		do {
			alarm[0] = 0;
			alarm[1] = 1;
			writeToBuffer(d_alarm, 0, 2, alarm);

			setConstantArg(level, 4);
			
			runKernel(nodes, max_work_group_size);

			readFromBuffer(d_alarm, 0, 2, alarm);

			level++;
		} while((alarm[0]) && (alarm[1]));

		// writeToFile<cl_uint>("cost.txt", d_F, nodes, 1);
		// writeToFile<cl_uint>("Parent.txt", d_P, nodes, 1);

		// return 1 if goal was found, else 0
		return !(alarm[1]);
	}

}
