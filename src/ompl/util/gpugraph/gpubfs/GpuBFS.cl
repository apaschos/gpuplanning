/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */

typedef struct
{
    uint weight;
    uint neighbour;
} node;


__kernel void bfs(__global const node* VE,
                __global uint* F,
                __global uint* P, // Parent
                __global uint* alarm,
                const uint level,
                const uint num_neighbs,
                const uint Vgoal);


////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


__kernel void bfs(__global const node* VE,
                __global uint* F,
                __global uint* P,
                __global uint* alarm,
                const uint level,
                const uint num_neighbs,
                const uint Vgoal)
{
    uint gid = get_global_id(0);
    uint offset, alrm = 0;
    node neib;

    if(F[gid] == level)
    {
        offset = gid*num_neighbs;

        for(uint i=0; i<num_neighbs; i++) {
            neib = VE[offset+i]; // distance and id of neighbour

            if(neib.weight == UINT_MAX) {
                    break;
            }
            alrm = 1;

            if(F[neib.neighbour] == UINT_MAX) {
                F[neib.neighbour] = level+1;
                P[neib.neighbour] = gid;
            }

            // if neighbour is the goal node, alarm everyone and stop the search
            if(neib.neighbour == Vgoal) {
                alarm[1] = 0;
                return;
            }
		}

        if(alrm) {
            alarm[0] = alrm;
        }
	}
}
