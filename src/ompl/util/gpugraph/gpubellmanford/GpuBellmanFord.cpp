/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */

#include "ompl/util/gpugraph/gpubellmanford/GpuBellmanFord.h"


namespace ompl{

    typedef enum {
    	SSSP,
    	FindParent
    } kernelFunctions;


	GpuBellmanFord::GpuBellmanFord(const cl_uint edges) : GpuGraph(edges)
	{
	    setup();
	}


	void GpuBellmanFord::setup(void)
	{

	    programSources = { loadSource("../src/ompl/util/gpugraph/gpubellmanford/GpuBellmanFord.cl") };
	    // programParams = { "" };
	    kernelNames = { {"sssp", "findParent"} };

	    GpuGraph::setup();
	}


	void GpuBellmanFord::sssp(cl::Buffer& d_Graph, const cl_uint Vstart)
	{
		cl_uint var;
		cl_uint alarm;
		cl::Event evt;
		nodes = getBufferSize<node>(d_Graph) / edges;

		d_Cost = CLBuffer<cl_uint>(CL_MEM_READ_WRITE, nodes);
		d_P = CLBuffer<cl_uint>(CL_MEM_READ_WRITE, nodes);
		d_alarm = CLBuffer<cl_uint>(CL_MEM_READ_WRITE, 1);

		in.initializeBuffer(d_Cost, CL_UINT_MAX);
		var = 0;
		writeToBuffer(d_Cost, Vstart, 1, &var);


		setCurrentKernel(SSSP);
		setGlobalArg(d_Graph);
		setGlobalArg(d_Cost);
		setGlobalArg(d_alarm);
		setConstantArg(edges);

		setCurrentKernel(FindParent);
		setGlobalArg(d_Graph);
		setGlobalArg(d_Cost);
		setGlobalArg(d_P);
		setConstantArg(edges);

		do
		{
			alarm = 0;
			writeToBuffer(d_alarm, 0, 1, &alarm);

			setCurrentKernel(SSSP);
			runKernel(nodes, max_work_group_size);
			readFromBuffer(d_alarm, 0, 1, &alarm, CL_FALSE, &evt);

			setCurrentKernel(FindParent);
			runKernel(nodes, max_work_group_size);

			evt.wait();

		} while(alarm);
	}

}
