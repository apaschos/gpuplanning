/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */

typedef struct
{
    uint weight;
    uint neighbour;
} node;


__kernel void sssp(__global const node* VE,
				__global uint* Cost,
				__global uint* alarm,
                const uint num_neighbs);

__kernel void findParent(__global const node* VE,
                __global uint* Cost,
                __global uint* Parent,
                const uint num_neighbs);


////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


__kernel void sssp(__global const node* VE,
                __global uint* Cost,
                __global uint* alarm,
                const uint num_neighbs)
{
	uint gid = get_global_id(0);
	uint offset, pcost, newcost;
	bool alrm;
    node neib;

    alrm = true;
    pcost = Cost[gid];
    offset = gid*num_neighbs;

    for(uint i=0; i<num_neighbs; i++) {
        neib = VE[offset+i]; // distance and id of neighbour

        if(neib.weight == UINT_MAX) {
                break;
        }

        newcost = pcost + neib.weight;
        if(atomic_min(&Cost[neib.neighbour], newcost) > newcost)
        {
        	alrm = true;
        }
    }

    if(alrm)
    {
    	alarm[0] = 1;
    }
}


__kernel void findParent(__global const node* VE,
				__global uint* Cost,
				__global uint* Parent,
                const uint num_neighbs)
{
	uint gid = get_global_id(0);
	uint offset, alrm, pcost, newcost;
    node neib;

    alrm = 0;
    pcost = Cost[gid];
    offset = gid*num_neighbs;

    for(uint i=0; i<num_neighbs; i++) {
        neib = VE[offset+i]; // distance and id of neighbour

        if(neib.weight == UINT_MAX) {
                break;
        }

        newcost = pcost + neib.weight;
        if(Cost[neib.neighbour] == newcost)
        {
        	Parent[neib.neighbour] = gid;
        	break;
        }
    }
}