/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */

#include "ompl/util/gpucompact/GpuCompact.h"
#include <cassert>


namespace ompl{

    typedef enum {
        Compact,
        Unique,
        StartCount
    } kernelFunctions;


    GpuCompact::GpuCompact() : OpenCL()
    {
        setup();

        uniqs = 0; // not really needed. just to avoid an accidental mistake
    }


    void GpuCompact::setup(void)
    {
        programSources = { loadSource("../src/ompl/util/gpucompact/GpuCompact.cl") };
        // programParams = { "" };
        kernelNames = { {"compact", "unique", "startCount"} };

        OpenCL::setup();
    }


    void GpuCompact::compact(cl::Buffer& d_Input, cl::Buffer& d_Diff, cl::Buffer& d_PSumDiff,
                cl::Buffer& d_Unique, const cl_uint columns, const cl_uint multiple,
                bool keepInitialSize)
    {
        const cl_uint rows = getBufferSize<cl_uint>(d_Input) / columns;


        // prefix sum on diff array
        sc.scan(d_Diff, d_PSumDiff);
        uniqs = sc.getTotal();
        assert(uniqs && "Number of unique elements should always be a non-negative number");

        if(!keepInitialSize) {
            d_Unique = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, uniqs * columns);
        }
        // otherwise it should be initialized by the user (same size with input)


        setCurrentKernel(Compact);
        setGlobalArg(d_Input);
        setGlobalArg(d_Diff);
        setGlobalArg(d_PSumDiff);
        setGlobalArg(d_Unique);
        setConstantArg(columns);
        setConstantArg(multiple);
        runKernel(rows, max_work_group_size);
    }


    void GpuCompact::unique(cl::Buffer& d_Input, cl::Buffer& d_Unique, cl::Buffer* d_Start,
                    cl::Buffer* d_Count, const cl_uint columns, const cl_uint multiple,
                    bool keepInitialSize)
    {
        const cl_uint rows = getBufferSize<cl_uint>(d_Input) / columns;

        cl::Buffer d_Diff = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, rows);
        cl::Buffer d_PSumDiff = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, rows);

        // d_Input elements must have been sorted first
        setCurrentKernel(Unique);
        setGlobalArg(d_Input);
        setGlobalArg(d_Diff);
        setConstantArg(columns);
        setConstantArg(multiple);
        runKernel(rows, max_work_group_size);

        compact(d_Input, d_Diff, d_PSumDiff, d_Unique, columns, multiple, keepInitialSize);


        if((d_Start != NULL) && (d_Count != NULL)) {
            *d_Start = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, uniqs);
            *d_Count = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, uniqs);

            // Compute the start and count arrays
            // d_Start refers to the index of the first occurrence of the key in input
            // d_Count refers to the number of occurrences of the key in input

            // First initialize d_Count with 1's
            // d_Count[i] is at least 1 for each i, because it has the length of uniqs
            // It helps avoiding some atomic_inc's on count array
            bfn.initializeBuffer(*d_Count, 1);


            setCurrentKernel(StartCount);

            setGlobalArg(d_Diff);
            setGlobalArg(d_PSumDiff);
            setGlobalArg(*d_Start);
            setGlobalArg(*d_Count);

            runKernel(rows, max_work_group_size);
        }
    }


    cl_uint GpuCompact::getNumUniques()
    {
        return uniqs;
    }


    void GpuCompact::unique(cl::Buffer& d_Input, cl::Buffer& d_Unique, bool keepInitialSize)
    {
        const cl_uint input_size = getBufferSize<cl_uint>(d_Input);

        unique(d_Input, d_Unique, NULL, NULL, 1, input_size, keepInitialSize);
    }


    void GpuCompact::unique(cl::Buffer& d_Input, cl::Buffer& d_Unique, cl::Buffer& d_Start,
                    cl::Buffer& d_Count, bool keepInitialSize)
    {
        const cl_uint input_size = getBufferSize<cl_uint>(d_Input);

        unique(d_Input, d_Unique, &d_Start, &d_Count, 1, input_size, keepInitialSize);
    }


    void GpuCompact::uniqueByKeys(cl::Buffer& d_Input, cl::Buffer& d_Unique, const cl_uint columns,
                    bool keepInitialSize)
    {
        const cl_uint input_size = getBufferSize<cl_uint>(d_Input);

        unique(d_Input, d_Unique, NULL, NULL, columns, input_size, keepInitialSize);
    }


    void GpuCompact::uniqueByKeys(cl::Buffer& d_Input, cl::Buffer& d_Unique, cl::Buffer& d_Start,
                    cl::Buffer& d_Count, const cl_uint columns, bool keepInitialSize)
    {
        const cl_uint input_size = getBufferSize<cl_uint>(d_Input);

        unique(d_Input, d_Unique, &d_Start, &d_Count, columns, input_size, keepInitialSize);
    }


    void GpuCompact::multipleUnique(cl::Buffer& d_Input, cl::Buffer& d_Unique, const cl_uint multiple,
                    bool keepInitialSize)
    {
        unique(d_Input, d_Unique, NULL, NULL, 1, multiple, keepInitialSize);
    }


    void GpuCompact::multipleUniqueByKeys(cl::Buffer& d_Input, cl::Buffer& d_Unique,
                    const cl_uint columns, const cl_uint multiple, bool keepInitialSize)
    {
        unique(d_Input, d_Unique, NULL, NULL, columns, multiple, keepInitialSize);
    }
}
