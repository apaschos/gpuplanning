/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */


/*
        -Compacts 
*/
__kernel void compact(__global const uint* Input,
                __global const uint* Diff,
                __global const uint* PSumDiff,
                __global uint* Output,
                const uint columns,
                const uint multiple);

/* Filters the unique elements of the input */
__kernel void unique(__global const uint* Input,
                __global uint* Diff,
                const uint columns,
                const uint multiple);

/*
	-Computes the start and count arrays
	-Start refers to the index of the first occurrence of the key in sorted lsh keys
	-Count refers to the number of occurrences of the key in sorted lsh keys
*/
__kernel void startCount(__global const uint* Diff,
                __global const uint* PSumDiff,
                __global uint* Start,
                __global uint* Count);


////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


__kernel void compact(__global const uint* Input,
                __global const uint* Diff,
                __global const uint* PSumDiff,
                __global uint* Output,
                const uint columns,
                const uint multiple)
{
    uint gid = get_global_id(0);
    uint pos = (gid/multiple*multiple) * columns;
    uint OutIndex, InIndex;

    if(Diff[gid] == 1) {
        OutIndex = (PSumDiff[gid] - PSumDiff[pos] + pos) * columns;
        InIndex = gid * columns;

        for(int i=0; i<columns; i++) {
            Output[OutIndex + i] = Input[InIndex + i];
        }
    }
}


__kernel void unique(__global const uint* Input,
                __global uint* Diff,
                const uint columns,
                const uint multiple)
{
    uint gid = get_global_id(0);
    uint pos = gid/multiple*multiple;

    // Since the input array is sorted, every thread compares its item with the previous
    // If they are the same, it writes 0, else 1

    if(gid == pos) { // First work item of each multiple doesn't have previous element
        Diff[gid] = 1;
    }
    else {
        pos = gid * columns;
        Diff[gid] = (Input[pos] == Input[pos-columns]) ? 0 : 1;
    }
}


__kernel void startCount(__global const uint* Diff,
                __global const uint* PSumDiff,
                __global uint* Start,
                __global uint* Count)
{
    uint gid = get_global_id(0);

    if(Diff[gid] == 1) {
        uint index = PSumDiff[gid];
        Start[index] = gid;
    }
    else {
        atomic_inc(&Count[PSumDiff[gid+1]-1]);
    }
}
