
/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */

////////////////////////////////////////
//         KEY           VALUE        //
//  --------------------------------  //
//  |      32      |      32       |  //
//  --------------------------------  //
////////////////////////////////////////
#define PACK(key, value) ((((ulong)key) << 32) | value)
#define GET_KEY(packet) ((uint)(packet >> 32))
#define GET_VALUE(packet) ((uint)(packet & 0xFFFFFFFF))


__kernel void build(__global const uint* Input,
				__global ulong* HashTable,
				__constant uint* Offsets,
				__global uint* alarm,
				const uint SIZE);


__kernel void retrieve(__global const uint* Keys,
				__global uint* Indices,
				__global const ulong* HashTable,
				__constant const uint* Offsets,
				const uint SIZE);


__inline uint coh_hash(const uint key,
				const uint ofset,
				const uint table_size);


////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


__kernel void build(__global const uint* Input,
				__global ulong* HashTable,
				__constant uint* Offsets,
				__global uint* alarm,
				const uint SIZE)
{
	uint gid = get_global_id(0);
	uint key, attempt, index;
	ulong packet, old;

	key = Input[gid];
	attempt = 0; // BUG: if key and value are both 0, the specific work-item is not inserted in the first position

	while(attempt < MAX_ATTEMPTS)
	{
		index = coh_hash(key, Offsets[attempt], SIZE);
		packet = PACK(key, gid);
		old = atomic_max(&HashTable[index], packet);
		
		if(packet > old)
		{
			if( old != 0)
			{
				key = GET_KEY(old);
			}
			else
			{
				return;
			}
		}
		else
		{
			attempt++;
		}
	}

	alarm[0] = 1;
}


__kernel void retrieve(__global const uint* Keys,
				__global uint* Indices,
				__global const ulong* HashTable,
				__constant const uint* Offsets,
				const uint SIZE)
{
	uint gid = get_global_id(0);
	uint key, age, max_age, index;
	ulong packet;

	key = Keys[gid];
	age = 0;

	while(true) // No MAX_AGE here
	{
		index = coh_hash(key, Offsets[age], SIZE);
		packet = HashTable[index];

		if(key == GET_KEY(packet))
		{
			Indices[gid] = GET_VALUE(packet);
			return;
		}
		else
		{
			age++;
		}
	}
}


__inline uint coh_hash(const uint key,
				const uint ofset,
				const uint table_size)
{
	uint index = (key + ofset) % table_size;

	return index;
}
