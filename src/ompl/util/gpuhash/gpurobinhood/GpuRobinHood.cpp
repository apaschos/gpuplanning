/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */

#include "ompl/util/gpuhash/gpurobinhood/GpuRobinHood.h"
#include <cassert>


namespace ompl{

    typedef enum {
	    Build,
        Retrieve,
        BuildMAT,
        RetrieveMAT
    } kernelFunctions;


    GpuRobinHood::GpuRobinHood(const cl_float load, const cl_uint blocksize) : GpuHash()
    {
        // check if atomic functions with 64 bit arguments are supported
        bool atomic64 = false;
        std::string extension_name = "cl_khr_int64_base_atomics";
        for(uint i=0; i<device_extensions.size(); i++)
        {
            if(device_extensions[i] == extension_name)
            {
                atomic64 = true;
                break;
            }
        }
        if(!atomic64) {
            // FOR SOME UNKNOWN WEIRD REASON ASSERT DOES NOT WORK HERE !!!!
            // assert(false && "GpuRobinHood kernels requires cl_khr_int64_base_atomics. Use GpuCuckoo as alternative.");
            exit(1);
        }
        

        this->blocksize = blocksize;
        this->load = 2-load;
        max_age = 16; // we use 4 bits for the age representation
        emptyKeys = false; // just a default value. change it through detectEmptyKeys()

        setup();
    }


    void GpuRobinHood::setup(void)
    {
        programSources = { loadSource("../src/ompl/util/gpuhash/gpurobinhood/GpuRobinHood.cl"),
                    loadSource("../src/ompl/util/gpuhash/gpurobinhood/GpuRobinHoodMAT.cl") };

        std::string params;
        params = std::string("-D MAX_ATTEMPTS=") + std::to_string(100);
        programParams.push_back(params);
        params = std::string("-D MAX_AGE=") + std::to_string(max_age);
        programParams.push_back(params);

        kernelNames = { {"build", "retrieve"}, {"buildMAT", "retrieveMAT"} };

        OpenCL::setup();
    }


    void GpuRobinHood::detectEmptyKeys(bool enabled)
    {
        emptyKeys = enabled;
    }


    int GpuRobinHood::build(cl::Buffer& d_Input)
    {
    	const cl_uint input_size = getBufferSize<cl_uint>(d_Input);
        const cl_uint new_size = input_size*load;
        cl_uint alarm;

        d_HashTable = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, new_size);
        d_Offsets = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, max_age);
        d_Alarm = CLBuffer<cl_uint>(CL_MEM_WRITE_ONLY, 1);

        // initialize offsets with random numbers
        std::vector<cl_uint> ofsts(max_age);
        rng.generateUniformNumbers(&ofsts[1], max_age-1, 0, CL_INT_MAX);
        ofsts[0] = 0;
        writeToBuffer(d_Offsets, 0, max_age, &ofsts[0]);
        alarm = 0;
        writeToBuffer(d_Alarm, 0, 1, &alarm);

        in.initializeBuffer(d_HashTable, 0);

        if(emptyKeys)
        {
            d_MAT = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, new_size);
            in.initializeBuffer(d_MAT, 0);

            setCurrentKernel(BuildMAT);
            setGlobalArg(d_Input);
            setGlobalArg(d_HashTable);
            setGlobalArg(d_MAT);
            setGlobalArg(d_Offsets);
            setGlobalArg(d_Alarm);
            setConstantArg(new_size);
            runKernel(input_size, blocksize); // TODO: take care when input is not a power of 2
        }
        else
        {
            setCurrentKernel(BuildMAT);
            setGlobalArg(d_Input);
            setGlobalArg(d_HashTable);
            setGlobalArg(d_Offsets);
            setGlobalArg(d_Alarm);
            setConstantArg(new_size);
            runKernel(input_size, blocksize); // TODO: take care when input is not a power of 2
        }

        readFromBuffer(d_Alarm, 0, 1, &alarm);

        return alarm;
    }


    void GpuRobinHood::retrieve(cl::Buffer& d_Keys, cl::Buffer& d_Values)
    {
        const cl_uint input_size = getBufferSize<cl_uint>(d_Keys);
        const cl_uint new_size = input_size*load;

        if(emptyKeys)
        {
            setCurrentKernel(RetrieveMAT);
            setGlobalArg(d_Keys);
            setGlobalArg(d_Values);
            setGlobalArg(d_HashTable);
            setGlobalArg(d_MAT);
            setGlobalArg(d_Offsets);
            setConstantArg(new_size);
            runKernel(input_size, blocksize);
        }
        else
        {
            setCurrentKernel(RetrieveMAT);
            setGlobalArg(d_Keys);
            setGlobalArg(d_Values);
            setGlobalArg(d_HashTable);
            setGlobalArg(d_Offsets);
            setConstantArg(new_size);
            runKernel(input_size, blocksize);
        }
    }
}
