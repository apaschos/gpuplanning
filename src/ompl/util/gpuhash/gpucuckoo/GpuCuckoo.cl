/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */


/* Computes each bucket's size and the offset of each key in the corresponding bucket */
__kernel void findBucket(__global const uint* keys,
				__global uint* offsets,
				__global uint* BCount,
				__global const uint2* randoms,
				__global ushort* alert,
				const uint buckets,
				const uint bucket_maxsize,
				const uint SIZE,
				const uint attempt);

/* Shuffles the input array into a new array which has each key with its corresponding index at the right bucket */
__kernel void shuffleData(__global const uint* keys,
				__global const uint* offsets,
				__global uint2* Shuffled,
				__global const uint* BStart,
				__constant uint2* rands,
				const uint buckets,
				const uint SIZE);

/* Builds the cuckoo table for each workgroup */
__kernel void buildCuckoo(__global uint2* outTable,
				__global const uint2* Shuffled,
				__global const uint* BCount,
				__global const uint* BStart,
				__constant ushort2* consts,
				__global ushort* bucket_seeds,
				__constant ushort* g_randoms,
				__global const uint2* deadTable,
				__local uint2* hltables,
				__global ushort* lucky_attempt,
				const uint local_table_size);

/* Retrieves for each key its index based on cuckoo table */
__kernel void retrieveCuckoo(__global const uint* keys,
				__global uint* indices,
				__global uint2* hashTable,
                __constant uint2* randoms,
                __constant ushort2* constants,
                __global const ushort* seeds,
                const uint local_table_size,
				const uint buckets);

/* Finds the right bucket in cuckoo first level for each key */
inline uint cuckooFindBucket(const uint key,
				const uint buckets,
				__constant uint2* rands);

/* Finds the index in cuckoo second level for each key */
inline uint cuckooFindIndex(const uint key,
				const uint local_table_size,
				const uint bucket,
				__global const uint2* hTable,
				__constant ushort2* constants,
				const ushort seed);

/* Generates the random numbers needed for the cuckoo second level hash functions */
inline void generateRandoms(const ushort seed,
				__constant ushort2* constants,
				ushort2* randoms);


////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


__kernel void findBucket(__global const uint* keys,
				__global uint* offsets,
				__global uint* BCount,
				__global const uint2* randoms,
				__global ushort* alert,
				const uint buckets,
				const uint bucket_maxsize,
				const uint SIZE,
				const uint attempt)
{
	uint gid = get_global_id(0);
	uint bucket_id, offset, key;

	if(gid < SIZE) {
		key = keys[gid];

		// Find the bucket id for each key based on the function h(k) = [(c0 + c1*k) % PRIME] % buckets
		bucket_id = ((randoms[attempt].x + randoms[attempt].y * key) % PRIME) % buckets;
		
		offset = atomic_inc(&BCount[bucket_id]);

		// Check if a bucket has exceeded the maximum size
		if(offset >= bucket_maxsize) {
			*alert = 1;
		}

		// Store each key's offset to an array for use in the next kernel
		offsets[gid] = offset;
	}
}


__kernel void shuffleData(__global const uint* keys,
				__global const uint* offsets,
				__global uint2* Shuffled,
				__global const uint* BStart,
				__constant uint2* rands,
				const uint buckets,
				const uint SIZE)
{
	uint gid = get_global_id(0);
	uint bucket_id, offset, key;


	if(gid < SIZE) {
		key = keys[gid];

		// Recompute the bucket_id
		bucket_id = ((rands[0].x + rands[0].y * key) % PRIME) % buckets;
		
		// Compute the global offset based on the bucket's start index and the local offset
		offset = BStart[bucket_id] + offsets[gid];

		// Store the key and its index in the new position (bucket aware)
		Shuffled[offset].x = key;
		Shuffled[offset].y = gid;
	}
}


__kernel void buildCuckoo(__global uint2* outTable,
				__global const uint2* Shuffled,
				__global const uint* BCount,
				__global const uint* BStart,
				__constant ushort2* consts,
				__global ushort* bucket_seeds,
				__constant ushort* g_randoms,
				__global const uint2* deadTable,
				__local uint2* hltables,
				__global ushort* lucky_attempt,
				const uint local_table_size)
{
	uint lid = get_local_id(0);
    uint groupid = get_group_id(0);
    const uint group_size = BCount[groupid]; // number of elements stored in this bucket
    const uint group_start = BStart[groupid]; // starting index for this bucket
    uint subtable_size = local_table_size / SUBTABLES;
    __local ushort2 newRandoms[SUBTABLES];
    __local bool alert;
    bool hashed;
    event_t evnt;
    ushort attempts;
    uint value_in, value_out, key_index, bucket_id, g_index, cur_ofst;


	// Each thread computes the index in the Shuffled array and picks its data
    if(lid < group_size) {
	    g_index = group_start + lid;
	    value_in = Shuffled[g_index].x;
	    key_index = Shuffled[g_index].y;
	}


    // This loop is where all threads in a workgroup start (or restart) the hashing procedure
	for(attempts=0; attempts<MAX_ATTEMPTS; attempts++) {

		// Compute the new randoms for this attempt and reset alert variable
		if(lid < SUBTABLES) {
			newRandoms[lid] = g_randoms[attempts] ^ consts[lid];
		}
		alert = false;
		barrier(CLK_LOCAL_MEM_FENCE);


		// Initialize the local table with 0xffffffff to perform cuckoo
		evnt = async_work_group_copy(hltables, deadTable, local_table_size, 0);
    	wait_group_events(1, &evnt);


		// Try to insert the key in one of the subtables until you succeed. Then set hashed to true and update the hltables
    	hashed = false; // Reset to false
    	// #pragma unroll
		for(ushort tries=0; tries<SUBTABLES; tries++) {
			if(lid<group_size && !hashed) {
				cur_ofst = tries * subtable_size; // The index at each subtable
				// Find the bucket_id in the subtable
				bucket_id = ((newRandoms[tries].x + newRandoms[tries].y * value_in) % PRIME) % subtable_size;
				cur_ofst += bucket_id; // Update the offset with the index in the subtable
				hltables[cur_ofst].x = value_in; // Insert your value and hope no one else overwrites it!
			}

			barrier(CLK_LOCAL_MEM_FENCE); // Synchronize all the workgroup threads so that the following read makes sense

			if(lid<group_size && !hashed) {
				value_out = hltables[cur_ofst].x; // Now check if your value was actually inserted
				if(value_in == value_out) { // If it was inserted, write the index too. Then loop aimlessly...
		        	hltables[cur_ofst].y = key_index;
		            hashed = true;
		        }
			}
    	}

		if(lid<group_size && !hashed) {
			alert = true;
		}

	    barrier(CLK_LOCAL_MEM_FENCE);

	    // If nobody has alerted failure, break and save the hashtable
		if(!alert) {
			break;
		}
	}


	// Store the attempt at which hashing worked and the corresponding random numbers for each workgroup
	if(lid == 0) {
		lucky_attempt[groupid] = attempts;
		bucket_seeds[groupid] = g_randoms[attempts];
	}

	// Copy the local hash table to the right place into the global hash table
	evnt = async_work_group_copy(outTable+(local_table_size*groupid), hltables, local_table_size, 0);
    wait_group_events(1, &evnt);
}


__kernel void retrieveCuckoo(__global const uint* keys,
				__global uint* indices,
				__global uint2* hashTable,
                __constant uint2* randoms,
                __constant ushort2* constants,
                __global const ushort* seeds,
                const uint local_table_size,
				const uint buckets)
{
	uint gid = get_global_id(0);
	uint hashkey = keys[gid];

	uint bucket = cuckooFindBucket(hashkey, buckets, randoms);
	indices[gid] = cuckooFindIndex(hashkey, local_table_size, bucket, hashTable, constants, seeds[bucket]);
}


inline uint cuckooFindBucket(const uint key,
				const uint buckets,
				__constant uint2* rands)
{
	uint2 rnd = *rands;

	return ((rnd.x + rnd.y * key) % PRIME) % buckets;
}


inline uint cuckooFindIndex(const uint key,
				const uint local_table_size,
				const uint bucket,
				__global const uint2* hTable,
				__constant ushort2* constants,
				const ushort seed)
{
	const uint subtable_size = local_table_size / SUBTABLES;
	const uint buckofst = bucket * local_table_size;
	uint index, offset, hash_index;
	ushort2 randoms[SUBTABLES];
	uint2 row;

	generateRandoms(seed, constants, randoms);

	index = UINT_MAX; // In case of empty key, return max uint
	for(uint i=0; i<SUBTABLES; i++) {
		offset = buckofst + i * subtable_size;

		hash_index = ((randoms[i].x + randoms[i].y * key) % PRIME) % subtable_size;

		row = hTable[offset + hash_index];
		if(row.x == key) {
			index = row.y;
			break;
		}
	}

	return index;
}


inline void generateRandoms(const ushort seed, __constant ushort2* constants, ushort2* randoms) {
	// #pragma unroll
	for(int i=0; i<SUBTABLES; i++) {
		randoms[i] = seed ^ constants[i];
	}
}
