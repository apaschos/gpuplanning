
/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */

#include "ompl/util/gpuhash/gpucuckoo/GpuCuckoo.h"


namespace ompl{

    typedef enum {
        FindBucket,
	    ShuffleData,
	    Build,
        Retrieve
    } kernelFunctions;


    GpuCuckoo::GpuCuckoo(const cl_float bucketLoad, const cl_float gamma,
    			const cl_uint max_attempts) : GpuHash()
    {
        setBucketLoadFactor(bucketLoad);
        setTotalLoadFactor(gamma);
        setMaximumAttempts(max_attempts);
        setSubtables(3);
        setPrime(1900813);
        // Add specific seeds if you want to test it
        setSeed1(0);
        setSeed2(0);

        setup();

        d_cuckoo1_Randoms = CLBuffer<cl_uint>(CL_MEM_READ_WRITE, 2);
        d_cuckoo2_constants = CLBuffer<cl_ushort>(CL_MEM_READ_ONLY, (subtables*2));
    }


    void GpuCuckoo::setup(void)
    {
        programSources = { loadSource("../src/ompl/util/gpuhash/gpucuckoo/GpuCuckoo.cl") };

        std::string params = "";
        params += std::string("-D SUBTABLES=") + std::to_string(subtables);
        params += std::string(" -D PRIME=") + std::to_string(primenum);
        params += std::string(" -D MAX_ATTEMPTS=") + std::to_string(max_attempts);
        programParams = { params };

        kernelNames = { {"findBucket", "shuffleData", "buildCuckoo", "retrieveCuckoo"} };

        OpenCL::setup();
    }


    int GpuCuckoo::build(cl::Buffer& d_Input)
    {
        /* Building is broken into two levels.
        In the first level, the elements of input buffer are distributed into buckets (whose size is
        max_work_group_size) based on a randomized hash function.
        In the second level, each bucket of the previous level is loaded in the local memory of a work group
        and the work group hashes the elements of this bucket, again based on a randomized hash function.
        Because of the randomized nature of the algorithm, building may fail in either level, so the whole
        process needs to restart. In case of succesful building, 0 is returned. In case of failure
        in first level, -1 is returned.In case of failure in second level, -2 is returned. */

    	const cl_uint input_size = getBufferSize<cl_uint>(d_Input);


        // Split the data into more buckets with fewer data in each
        cl_uint block_load = static_cast<cl_uint>(max_work_group_size * bucketLoad); // each bucket gets a percentage of the workgroup size
    	buckets = input_size/block_load + ((input_size%block_load) ? 1 : 0); // compute the number of buckets and round up 

        // Initialize the device buffers
        cl::Buffer d_Alert = CLBuffer<cl_ushort>(CL_MEM_WRITE_ONLY, 1); // indicates that cuckoo hashing has failed and all threads should restart
        cl::Buffer d_Shuffled = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, (2 * input_size)); // stores the shuffled data (value, index) from the first level cuckoo
        cl::Buffer d_BuckCount = CLBuffer<cl_uint>(CL_MEM_READ_WRITE, buckets); // stores the number of items that fall in each bucket
        cl::Buffer d_BuckStart = CLBuffer<cl_uint>(CL_MEM_READ_WRITE, buckets); // stores the starting position of each bucket in the shuffled array
        cl::Buffer d_Offsets = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, input_size); // auxiliary buffer for the first level cuckoo hashing
        cl::Buffer d_Randoms = CLBuffer<cl_uint>(CL_MEM_READ_ONLY, (2 * max_attempts)); // keeps the random numbers used for the hashing
        cl::Buffer d_lucky_attempt; // keeps the attempt at which all threads succeeded in hashing
        cl::Buffer d_InitTable;


        // First level cuckoo hashing

        cl_ushort alert, attempt;
        cl_uint* c = new cl_uint[2*max_attempts];
        // Generate random numbers
        rng.setSeed(cuckoo1_seed);
        rng.generateUniformNumbers<cl_uint>(c, 2*max_attempts, 0, CL_INT_MAX);
        // The first two are not random
        c[0] = 0;
        c[1] = 1;
        
        // Write the random numbers to the device
        writeToBuffer(d_Randoms, 0, 2*max_attempts, c);
		delete[] c;


        setCurrentKernel(FindBucket);
        setGlobalArg(d_Input);
        setGlobalArg(d_Offsets);
        setGlobalArg(d_BuckCount);
        setGlobalArg(d_Randoms);
        setGlobalArg(d_Alert);
        setConstantArg(buckets);
        setConstantArg(max_work_group_size);
        setConstantArg(input_size);

        for(attempt=0; attempt<max_attempts; attempt++) {
            // Initialize BuckCount with 0's
            in.initializeBuffer(d_BuckCount, 0);

            alert = 0;
            writeToBuffer(d_Alert, 0, 1, &alert);

            setConstantArg(attempt, 8);
            runKernel(input_size);

            readFromBuffer(d_Alert, 0, 1, &alert);
            if(alert==0) {
                break;
            }
        }

        if(attempt == max_attempts) {
            return -1;
        }

        copyBetweenBuffers<cl_uint>(d_Randoms, d_cuckoo1_Randoms, 2*attempt, 0, 2);

        // prefix sum on BucketCount to determine Bucket start offsets
        sc.scan(d_BuckCount, d_BuckStart); // prefix sum on BCount
        

        setCurrentKernel(ShuffleData);
        setGlobalArg(d_Input);
        setGlobalArg(d_Offsets);
        setGlobalArg(d_Shuffled);
        setGlobalArg(d_BuckStart);
        setGlobalArg(d_cuckoo1_Randoms);
        setConstantArg(buckets);
        setConstantArg(input_size);
        runKernel(input_size);

        //---------------------------------------------------------------------------------------------------------

        // Second level cuckoo hashing

        // Compute the size of the table each workgroup will use
        localTableSize = static_cast<cl_uint>(max_work_group_size * (1+gamma));
        
        for(uint i=0; i<subtables; i++) { // turn localTableSize into a number devidable by subtables
            if(localTableSize % subtables != 0) localTableSize--;
            else break;
        }


        d_InitTable = CLBuffer<cl_uint>(CL_MEM_READ_ONLY, (localTableSize * 2)); // used for initializing the local table with 0xffffffff at each attempt
        d_Randoms = CLBuffer<cl_ushort>(CL_MEM_READ_WRITE, max_attempts);
        d_HashTable = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, (2 * buckets * localTableSize)); // the final hash table
        d_InitTable = CLBuffer<cl_uint>(CL_MEM_READ_ONLY, (localTableSize * 2)); // used for initializing the local table with 0xffffffff at each attempt
        d_Randoms = CLBuffer<cl_ushort>(CL_MEM_READ_WRITE, max_attempts);
        d_cuckoo2_seeds = CLBuffer<cl_ushort>(CL_MEM_GPU_ONLY, buckets); // will keep the lucky seed for each bucket
        d_lucky_attempt = CLBuffer<cl_ushort>(CL_MEM_WRITE_ONLY, buckets); // in the second level each workgroup has a lucky run

        // Initialize d_InitTable with CL_UINT_MAX
        in.initializeBuffer(d_InitTable, CL_UINT_MAX);

        // Generate the constants and the seeds for the second level cuckoo
		cl_ushort* constants = new cl_ushort[subtables*2];
		cl_ushort* seeds = new cl_ushort[max_attempts];
        rng.setSeed(cuckoo2_seed);
        rng.generateUniformNumbers<cl_ushort>(constants, 2*subtables, 0, CL_USHRT_MAX);
        rng.generateUniformNumbers<cl_ushort>(seeds, max_attempts, 0, CL_USHRT_MAX);

        writeToBuffer(d_cuckoo2_constants, 0, (subtables*2), constants);
        writeToBuffer(d_Randoms, 0, (max_attempts), seeds);

		delete[] constants;
		delete[] seeds;


        setCurrentKernel(Build);
        setGlobalArg(d_HashTable);
        setGlobalArg(d_Shuffled);
        setGlobalArg(d_BuckCount);
        setGlobalArg(d_BuckStart);
        setGlobalArg(d_cuckoo2_constants);
        setGlobalArg(d_cuckoo2_seeds);
        setGlobalArg(d_Randoms);
        setGlobalArg(d_InitTable);
        setLocalArg(localTableSize * 2 * sizeof(cl_uint));
        setGlobalArg(d_lucky_attempt);
        setConstantArg(localTableSize);
        runKernel((buckets * max_work_group_size), max_work_group_size);

        cl_ushort* attempts = new cl_ushort[buckets];
        readFromBuffer(d_lucky_attempt, 0, buckets, attempts);
        
        for(cl_uint i=0; i<buckets; i++) {
            if(attempts[i] == max_attempts) { // if it fails, exit
                delete[] attempts;
                return -2;
            }
        }

        delete[] attempts;

        // writeToFile<cl_uint>("hashTable.txt", d_HashTable, buckets*localTableSize, 2);
        return 0;
    }


    void GpuCuckoo::retrieve(cl::Buffer& d_Keys, cl::Buffer& d_Values)
    {
        const cl_uint input_size = getBufferSize<cl_uint>(d_Keys);


        setCurrentKernel(Retrieve);
        setGlobalArg(d_Keys);
        setGlobalArg(d_Values);
        setGlobalArg(d_HashTable);
        setGlobalArg(d_cuckoo1_Randoms);
        setGlobalArg(d_cuckoo2_constants);
        setGlobalArg(d_cuckoo2_seeds);
        setConstantArg(localTableSize);
        setConstantArg(buckets);
        runKernel(input_size);
    }


    void GpuCuckoo::setSeed1(cl_uint sd)
    {
        cuckoo1_seed = sd;
    }


    void GpuCuckoo::setSeed2(cl_uint sd)
    {
        cuckoo2_seed = sd;
    }


    void GpuCuckoo::setSubtables(cl_uint num)
    {
        subtables = num;
    }


    void GpuCuckoo::setPrime(cl_uint prime)
    {
        primenum = prime;
    }


    void GpuCuckoo::setBucketLoadFactor(cl_float load)
    {
        bucketLoad = load;
    }


    void GpuCuckoo::setTotalLoadFactor(cl_float load)
    {
        gamma = load;
    }


    void GpuCuckoo::setMaximumAttempts(cl_uint num)
    {
        max_attempts = num;
    }


    cl_uint GpuCuckoo::getSeed1()
    {
        return cuckoo1_seed;
    }


    cl_uint GpuCuckoo::getSeed2()
    {
        return cuckoo2_seed;
    }


    cl_uint GpuCuckoo::getSubtables()
    {
        return subtables;
    }


    cl_uint GpuCuckoo::getPrime()
    {
        return primenum;
    }


    cl_float GpuCuckoo::getBucketLoadFactor()
    {
        return bucketLoad;
    }


    cl_float GpuCuckoo::getTotalLoadFactor()
    {
        return gamma;
    }


    cl_uint GpuCuckoo::getMaximumAttempts()
    {
        return max_attempts;
    }


}