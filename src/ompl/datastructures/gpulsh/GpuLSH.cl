/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */


/* Calculates the hash key for each milestone based on the locality sensitive hashing */
__kernel void localitySensitiveHashing(__global const uint* Input,
            __global uint* Output,
            __constant float* Weights,
            __constant float* a,
            __constant uint* b,
            __constant uint* r);

__kernel void findNeighbs(__global uint2* tempKNN,
					__global const uint* Input,
					__global const uint* Indices,
					__global const uint* InvPerm,
	                __global const uint* Start,
	                __global const uint* Count,
	                const uint workspace_size,
	                const uint runOffset);

inline float getDistance(const uint* milestone1, const uint* milestone2);

inline void insert(uint* ref_mile,
				uint* cur_mile,
				__global uint2* bestNN,
				const uint cur_id,
				const uint size);


////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


__kernel void localitySensitiveHashing(__global const uint* Input,
            __global uint* Output,
            __constant float* Weights,
            __constant float* a,
            __constant uint* b,
            __constant uint* r) {

	uint gid = get_global_id(0);
	uint hu = 0;
	uint g; // g(q) = <h1(q), ..., hM(q)>
	float h_m;

	for(uint m=0; m<M; m++) {
		h_m = 0.0;
		// #pragma unroll
		for(uint d=0; d<DIMS; d++) {
			h_m += a[m*DIMS+d] * (Weights[d] * Input[gid*DIMS+d]);
		}
		h_m += convert_float(b[m]);
		g = convert_uint( floor(h_m / W) );
		hu += g * r[m];
	}

	Output[gid] = hu;
}


__kernel void findNeighbs(__global uint2* tempKNN,
					__global const uint* Input,
					__global const uint* Indices,
					__global const uint* InvPerm,
	                __global const uint* Start,
	                __global const uint* Count,
	                const uint workspace_size,
	                const uint runOffset)
{
	uint gid = get_global_id(0);
	uint startingOffset = gid * workspace_size + runOffset;
	uint bucket, offset, uniqs_offset, strt, cnt, lim, cur_size, orig_indx;
	uint referencemile[DIMS]; // the milestone for which we search the k nearest neighbours
	uint mile[DIMS]; // the milestone found at each iteration


	// Copy the milestone from global to private memory
	// #pragma unroll
	for(uint d=0; d<DIMS; d++) {
		referencemile[d] = Input[gid+d];
	}

	uniqs_offset = Indices[gid];
	strt = Start[uniqs_offset]; // The index at sorted Input where the lsh key first appears
	cnt = Count[uniqs_offset]; // The number of times this key appears in sorted Input
	lim = strt+cnt; // used just to avoid the recomputation of strt+cnt at every iteration of the following loop
	cur_size = 0;

	for(uint i=strt; i<lim; i++) {
		orig_indx = InvPerm[i];

		if(gid == orig_indx) { // We don't want the identical milestone to be stored as neighbour
			continue;
		}

		// Copy milestone from global to private memory
		offset = orig_indx * DIMS;
		// #pragma unroll
		for(uint d=0; d<DIMS; d++) {
			mile[d] = Input[offset+d];
		}

		insert(referencemile, mile, tempKNN+startingOffset, orig_indx, cur_size++);
	}
}


inline float getDistance(const uint* milestone1, const uint* milestone2) {
	uint squaresum;
	uint diff;

	squaresum = 0;
	// #pragma unroll
	for(uint i=0; i<DIMS; i++) {
		diff = milestone2[i] - milestone1[i];
		squaresum += diff * diff;
	}

	return sqrt(convert_float(squaresum));
}


inline void insert(uint* ref_mile,
				uint* cur_mile,
				__global uint2* bestNN,
				const uint cur_id,
				const uint size)
{
	const uint L2 = convert_uint(getDistance(ref_mile, cur_mile));
	uint2 val;
	uint temp;
	uint highestdistance;
	int distant;

	if(size<extraK) {
		val.x = L2;
		val.y = cur_id;
		bestNN[size] = val;
	}
	/*else {
		distant = -1;
		highestdistance = L2;
		for(uint i=0; i<extraK; i++) {
			temp = bestNN[i].x;
			if(temp > highestdistance) {
				highestdistance = temp;
				distant = i;
			}
		}

		if(distant != -1) { // if there's actually a milestone with bigger L2, replace it
			val.x = L2;
			val.y = cur_id;
			bestNN[distant] = val;
		}
	}*/
}
