/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */

#include "ompl/datastructures/gpulsh/GpuLSH.h"
#include "ompl/util/gpurng/GpuRandomNumbers.h"
#include <cassert>


namespace ompl{

    typedef enum {
        LSH,
        FindNeighbs
    } kernelFunctions;


    GpuLSH::GpuLSH(const config robot, const cl_uint K, const cl_uint L,
                const cl_uint M, const cl_uint W) : OpenCL(), dimensions_(robot.dimensions),
                K_(K), L_(L), M_(M), W_(W)
    {
        // User MUST tweak delta to make sure extraK is a multiple of chunk (see radix sort)
        setDelta(0.6);

        rdx = {1, 2, 32};
        // rng = {111};
        cck = {0.7, 0.8};

        setup();

        d_a = CLBuffer<cl_float>(CL_MEM_READ_ONLY, (M_ * dimensions_));
        d_b = CLBuffer<cl_uint>(CL_MEM_READ_ONLY, M_);
        d_r = CLBuffer<cl_uint>(CL_MEM_READ_ONLY, M_);

        // Copy weight vector to gpu
        d_Weights = CLBuffer<cl_float>(CL_MEM_READ_ONLY, dimensions_);
        writeToBuffer<cl_float>(d_Weights, 0, dimensions_, (cl_float *) &robot.weights[0]);

        // Initialize seeds vector with random data
        seeds = std::vector<cl_uint>(L_);
        rng.generateUniformNumbers<cl_uint>((cl_uint *) &seeds[0], L_, 0, 100); // should make this more configurable and generic
    }


    void GpuLSH::setup(void)
    {
        programSources = { loadSource("../src/ompl/datastructures/gpulsh/GpuLSH.cl") };
        std::string params = "";
        params += std::string("-D DIMS=") + std::to_string(dimensions_);
        params += std::string(" -D K=") + std::to_string(K_);
        params += std::string(" -D L=") + std::to_string(L_);
        params += std::string(" -D M=") + std::to_string(M_);
        params += std::string(" -D W=") + std::to_string(W_*1.0f);
        params += std::string(" -D extraK=") + std::to_string(extraK);
        programParams = { params };
        kernelNames = { {"localitySensitiveHashing", "findNeighbs"} };

        OpenCL::setup();
    }


    // make sure to run setup() again so that changes affect the gpu code
    void GpuLSH::setDelta(cl_float num)
    {
        extraK = K_ * (1+num);
        while(max_work_group_size % extraK) {
            extraK++;
        }
        delta = (extraK * 1.0f / K_) - 1; // calculate new delta
    }


    void GpuLSH::calculateLSH(cl::Buffer& d_Input, cl::Buffer& d_Output, uint seed)
    {
        const cl_uint input_size = getBufferSize<cl_uint>(d_Input) / dimensions_;

        cl_uint* r = new cl_uint[M_];
        cl_uint* b = new cl_uint[M_];
        cl_float* a = new cl_float[M_*dimensions_];


        // Generate random data and copy them to device buffers
        rng.setSeed(seed);
        rng.generateGaussianNumbers<cl_float>(a, M_*dimensions_, 0.0f, 1.0f, true);
        rng.generateUniformNumbers<cl_uint>(b, M_, 0, W_);
        rng.generateUniformNumbers<cl_uint>(r, M_, 0, 32768);

        writeToBuffer(d_a, 0, (M_ * dimensions_), a);
        writeToBuffer(d_b, 0, M_, b);
        writeToBuffer(d_r, 0, M_, r);


        setCurrentKernel(LSH);
        setGlobalArg(d_Input);
        setGlobalArg(d_Output);
        setGlobalArg(d_Weights);
        setGlobalArg(d_a);
        setGlobalArg(d_b);
        setGlobalArg(d_r);
        runKernel(input_size, max_work_group_size);


        delete[] r;
        delete[] b;
        delete[] a;
    }


    void GpuLSH::nearestK(cl::Buffer& d_Input, cl::Buffer& d_KNN)
    {
        const cl_uint items = getBufferSize<cl_uint>(d_Input) / dimensions_;
        // cl_uint uniqitems;
        cl_uint runOffset;
        cl_uint workspace_size;
        cl_uint loadMultiple;
        cl_uint ct;
        int res;

        cl::Buffer d_LshKeys = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, items);
        cl::Buffer d_SortedLshKeys = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, items);
        cl::Buffer d_PermArray = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, items);
        cl::Buffer d_InvPermArray = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, items);
        cl::Buffer d_UniqKeys;
        cl::Buffer d_Start;
        cl::Buffer d_Count;
        cl::Buffer d_Indices = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, items);
        cl::Buffer d_KNNHeap;



        // Each work-item uses a workspace to hold the generated neighbours.
        // At each l run, extraK neighbours are kept (instead of only K).
        // Instead of processing (aka sorting and keeping best unique) these neighbours 
        // at each l run,we process them at a slower rate.
        // The workspace variable keeps the total of all these neighbours to be processed.
        // When the workspace is filled with neighbours, it starts processing them.
        loadMultiple = L_>4? 8:4; // keeps how much bigger than extraK the workspace will be
        workspace_size = loadMultiple * extraK;
        assert((items * workspace_size * 2 <= global_mem_size)
                    && "Total size of d_KNNHeap buffer should not exceed maximum global memory size");

        d_KNNHeap = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, items * workspace_size * 2);
        d_KNN = CLBuffer<cl_uint>(CL_MEM_GPU_ONLY, items * K_ * 2);

        setCurrentKernel(FindNeighbs);
        setGlobalArg(d_KNNHeap);
        setGlobalArg(d_Input);
        setGlobalArg(d_Indices);
        setConstantArg(workspace_size, 6);


        // Initialize d_KNNHeap with MAX UINT (only the key)
        bfn.initializeBuffer_KeysOnly(d_KNNHeap, CL_UINT_MAX, 2);

        for(uint l=0; l<L_; l++)
        {
            calculateLSH(d_Input, d_LshKeys, seeds[l]);

            // Sort milestones and keep permutation
            rdx.sortPermute(d_LshKeys, d_SortedLshKeys, d_PermArray);
            rdx.computeInversePermutation(d_PermArray, d_InvPermArray);

            // Find the unique and also the start and count arrays in d_SortedLshKeys
            unq.unique(d_SortedLshKeys, d_UniqKeys, d_Start, d_Count);
            // uniqitems = unq.getNumUniques();

            // Build the hash table and then retrieve for each milestone its index in d_UniqKeys
            ct = 0;
            while((res = cck.build(d_UniqKeys)) && ((ct++)<20));
            assert(!res && "Cuckoo building failed");
            cck.retrieve(d_LshKeys, d_Indices);


            // Each work-item keeps the first extraK items of its workspace unused
            // (and initialized with MAX UINT distance). It places new neighbours after 
            // them so that, after sorting, the best neighbours are kept in the first extraK.
            // runOffset calculates the offset at which the work-item will start 
            // placing the neighbours at its workspace.
            runOffset = (l % (loadMultiple-1) + 1) * extraK;

            setCurrentKernel(FindNeighbs);
            setGlobalArg(d_InvPermArray, 3);
            setGlobalArg(d_Start, 4);
            setGlobalArg(d_Count, 5);
            setConstantArg(runOffset, 7);
            runKernel(items, max_work_group_size);

            // If workspace of work-item is full of data or this is the last l, sort neighbours before going on
            if(((l % (loadMultiple-1) + 1) == (loadMultiple-1)) || (l == L_-1)) {
                rdx.multipleSortByKeys(d_KNNHeap, 2, workspace_size);
            }
        }


        // Finally store the best to d_KNN

        // Just to remove some repeated calculations in the loop
        cl_uint src_size = workspace_size*2;
        cl_uint dst_size = K_*2;
        // We do the transfers asynchronously because they are not dependent (each work-item has its own workspace)
        for(cl_uint i=0; i<items; i++) {
            copyBetweenBuffers<cl_uint>(d_KNNHeap, d_KNN,
                i*src_size, i*dst_size, dst_size);
        }
        waitQueue(); // we wait for all the transfers to finish
    }
}
