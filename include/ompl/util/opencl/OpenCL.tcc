/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/


/* Author: Andrew Paschos */


#include <fstream> // uncomment it only if writeToFile() is not commented


namespace ompl
{
	template <typename T>
    void OpenCL::writeToBuffer(cl::Buffer& buffer, cl_uint offset, cl_uint size, T* cpuArray, cl_bool blocking, cl::Event* event) {
        cl_uint typesize = sizeof(T);
        // check if Buffer mem flags permit this operation
        queue.enqueueWriteBuffer(buffer, blocking, offset * typesize, size * typesize, cpuArray, NULL, event);
    }


    template <typename T>
    void OpenCL::readFromBuffer(const cl::Buffer& buffer, cl_uint offset, cl_uint size, T* cpuArray, cl_bool blocking, cl::Event* event) {
        cl_uint typesize = sizeof(T);
        // check if Buffer mem flags permit this operation
        queue.enqueueReadBuffer(buffer, blocking, offset * typesize, size * typesize, cpuArray, NULL, event);
    }


    template <typename T>
    void OpenCL::copyBetweenBuffers(cl::Buffer& bsrc, cl::Buffer& bdst, cl_uint src_offset,
                cl_uint dst_offset, cl_uint size, cl_bool blocking)
    {
        cl_uint typesize = sizeof(T);

        queue.enqueueCopyBuffer(bsrc, bdst, src_offset * typesize, dst_offset * typesize, size * typesize);

		if(blocking == CL_TRUE) {
			waitQueue();
		}
    }


    template <typename T>
    cl::Buffer OpenCL::CLBuffer(cl_mem_flags flags, cl_uint size) {
        return cl::Buffer(context, flags, size * sizeof(T));
    }


    template <typename T>
    cl::Buffer* OpenCL::CLBufferPtr(cl_mem_flags flags, cl_uint size)
    {
        return (new cl::Buffer(context, flags, size * sizeof(T)));
    }


    template <typename T>
    cl::Buffer OpenCL::SubBuffer(cl::Buffer& origin, cl_uint index, cl_uint size) {
        cl_buffer_region bregion =
        {
            index * sizeof(T),
            size * sizeof(T)
        };

        return origin.createSubBuffer(origin.getInfo<CL_MEM_FLAGS>(), CL_BUFFER_CREATE_TYPE_REGION, &bregion);
    }


    template <typename T>
    cl_uint OpenCL::getBufferSize(const cl::Buffer& buf) {
        return  buf.getInfo<CL_MEM_SIZE>() / sizeof(T);
    }


    template <typename T>
    void OpenCL::writeToFile(std::string filename, cl::Buffer& data, cl_uint dim1, cl_uint dim2) {
        cl::Buffer newdata;
        T *temp = new T[dim1*dim2];

        // Check if Buffer's memory flags permit this operation
        #if(__OPENCL_VERSION__ >= 120)
        if(!(data.getInfo<CL_MEM_FLAGS>() &&  CL_MEM_HOST_READ_ONLY)) {
            newdata = CLBuffer<T>(CL_MEM_HOST_READ_ONLY, dim1*dim2);
            copyBetweenBuffers<T>(data, newdata, 0, 0, dim1*dim2);
            readFromBuffer<T>(newdata, 0, (dim1*dim2), temp);
        }
        else {
            readFromBuffer<T>(data, 0, (dim1*dim2), temp);
        }
        #else
        readFromBuffer<T>(data, 0, (dim1*dim2), temp);
        #endif

        std::ofstream filehandler;
        filehandler.open(filename.c_str());
        for(cl_uint i=0; i<dim1; i++) {
            for(cl_uint j=0; j<dim2-1; j++) {
                filehandler << temp[i*dim2+j] << "\t";
            }
            filehandler << temp[(i+1)*dim2-1] << std::endl;
        }
        filehandler.close();

        delete[] temp;
    }


    template <typename T>
    void OpenCL::readFromFile(std::string filename, cl::Buffer& data, cl_uint dim1, cl_uint dim2) {
        T* temp = new T[dim1*dim2];

        std::ifstream filehandler(filename.c_str());
        std::string line;
        std::string::size_type pos, last_pos;

        for(cl_uint i=0; i<dim1; i++)
        {
            if(!std::getline(filehandler, line))
            {
                break;
            }

            last_pos = 0;
            for(cl_uint j=0; j<dim2; j++)
            {
                pos = line.find_first_of("\t", last_pos);
                if(pos == std::string::npos)
                {
                    pos = line.length();

                    temp[i*dim2+j] = std::stoul(std::string(line, last_pos, (pos-last_pos)));

                    break;
                }
                else
                {
                    temp[i*dim2+j] = std::stoul(std::string(line, last_pos, (pos-last_pos)));
                    last_pos = pos + 1;
                }
            }
        }

        writeToBuffer<T>(data, 0, dim1*dim2, temp);

        delete[] temp;
    }
}