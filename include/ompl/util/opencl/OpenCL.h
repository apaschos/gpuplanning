/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/


#ifndef OMPL_UTIL_OPENCL_OPENCL_
#define OMPL_UTIL_OPENCL_OPENCL_


/*! \mainpage Gpu Ompl
 *
 * \section intro_sec Introduction
 *
 * OpenCL implementation of Ompl algorithms. @n
 * For more info, check @n
 * <a href="http://ompl.kavrakilab.org/">Ompl</a> @n
 * <a href="http://www.khronos.org/opencl/">OpenCL</a> @n
 * Tested under Ubuntu 12.04 with OpenCL 1.1 and 1.2 on Nvidia and AMD GPUs.
 *
 * \section Build
 *
 * mkdir build @n
 * cd build @n
 * cmake .. @n
 * make (-j4) @n
 * 
 */

/*! \file OpenCL.h
 *  \brief Wraps around the official OpenCL header cl.hpp.
 *  
 *  \details Provides a simplified interface to work with OpenCL.
 *  This is the base class from which the rest inherit.
 *  As an alternative, use directly the CL/cl.hpp header.
 *  
 *  \author Andrew Paschos
 *  
 *  \example ocl.cpp
 *  This is an example of how to create your own class with gpu code.
 */


#ifndef UINT
 /** Alias for unsigned int */
typedef unsigned int uint;
#endif

#ifndef ULONG
 /** Alias for unsigned long */
typedef unsigned long ulong;
#endif

/** Enable OpenCL exceptions */
#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp>

/** \brief Main namespace. Contains everything in this library. */
namespace ompl {

	// Memory flag for creating buffers on gpu only (in case of OpenCL 1.2)
    namespace {
        #if(__OPENCL_VERSION__ >= 120)
        /**
         * \brief Short name for gpu only memory flags 
         */
        const cl_mem_flags CL_MEM_GPU_ONLY = CL_MEM_HOST_NO_ACCESS;
        #else
        /**
         * \brief Short name for gpu only memory flags 
         */
        const cl_mem_flags CL_MEM_GPU_ONLY = CL_MEM_READ_WRITE;
        #endif
    }

    /**
     * \brief Enumerator for device types
     */
    typedef enum {
        DEFAULT = CL_DEVICE_TYPE_DEFAULT,
        CPU = CL_DEVICE_TYPE_CPU,
        GPU = CL_DEVICE_TYPE_GPU,
        ACCELERATOR = CL_DEVICE_TYPE_ACCELERATOR,
        ALL = CL_DEVICE_TYPE_ALL
    } Type;

    /**
     * \brief Enumerator for the main device vendors
     */
    typedef enum {
        AMD,
        NVIDIA,
        INTEL,
        OTHER
    } Vendor;

    /** \class OpenCL
     * \brief OpenCL class
     * 
     * \details The base class from which all opencl algorithms inherit. Wraps around CL/cl.hpp.
     */
	class OpenCL {

	public:

        /**
         * \brief Default Constructor
         * \details Checks if OpenCL is initialized.
         */
		OpenCL();

        /**
         * \brief Initialize OpenCL. This method SHOULD be called in main before using any OpenCL structure.
         * \details Initializes the data structures of OpenCL. @n
         * These include creating Platform, Context, Device and CommandQueue. @n
         * Currently only one of each is supported (for ease of use). @n
         * Notice that each structure is shared among all OpenCL objects.
         * @param[in] platformIndex The index of the desired platform
         * @param[in] devType The type of device to search for
         * @param[in] devIndx The index of the desired device
         */
		static void initOpenCL(uint platformIndex = 0, Type devType = GPU, uint devIndx = 0);

        /**
         * \brief List device capabilities
         */
        static void deviceQuery();

        #ifdef PROFILING
        /**
         * \brief Get the execution time of the last event
         * @return The time in nanoseconds
         */
        cl_ulong getEventTime();
        #endif

        /**
         * \brief Create a gpu buffer.
         * \details Create a gpu buffer (gpu memory block) with the specified flags and size. @n
         * Wraps around Buffer() method.
         * 
         * @param flags Buffer flags
         * @param size The size of the buffer
         * @tparam T Type of data
         * @return A Buffer object
         */
        template <typename T> static cl::Buffer CLBuffer(cl_mem_flags flags, cl_uint size);

        /**
         * \brief Create a gpu buffer pointer.
         * \details Create a gpu buffer (gpu memory block) with the specified flags and size and return a pointer to the object. @n
         * Wraps around Buffer() method.
         * 
         * @param flags Buffer flags
         * @param size The size of the buffer
         * @tparam T Type of data
         * @return A pointer to a Buffer object
         */
        template <typename T> static cl::Buffer* CLBufferPtr(cl_mem_flags flags, cl_uint size);

        /**
         * \brief Create a buffer from an already allocated gpu buffer.
         * \details Create a buffer from the origin buffer with the specified size. @n
         * No extra memory is allocated. The new buffer points to the index of the origin buffer. @n
         * Size should NOT exceed the size of the origin buffer. @n
         * Wraps around Buffer() method.
         * 
         * @param[out] origin The buffer to store the data. 
         * @param index The position at which the pointer should point
         * @param size The size of the buffer
         * @tparam T Type of data
         * @return A Buffer object
         */
        template <typename T> static cl::Buffer SubBuffer(cl::Buffer& origin, cl_uint index, cl_uint size);

        /**
         * \brief Transfer cpu array to gpu buffer.
         * \details Transfer cpu array to gpu buffer. @n
         * Blocking and non-blocking mode are available. @n
         * If non-blocking, use event to query the state of the transfer. @n
         * 
         * @param[out] buffer The buffer to store the data. Should be initialized and have size of at least size
         * @param[in] offset The offset of the cpu array at which the transfer starts
         * @param[in] size The size of the data to be trasfered
         * @param[in] cpuArray Pointer to the cpu array
         * @param[in] blocking CL_TRUE to wait for transfer to finish. CL_FALSE to continue without waiting
         * @param[out] event Event object to use in non-blocking mode to query the state of the transfer
         * @tparam T Type of data
         */
        template <typename T> static void writeToBuffer(cl::Buffer& buffer, cl_uint offset,
            cl_uint size, T* cpuArray, cl_bool blocking = CL_TRUE, cl::Event* event = NULL);

        /**
         * \brief Transfer gpu buffer to cpu array.
         * \details Transfer gpu buffer to cpu array. @n
         * Blocking and non-blocking mode are available. @n
         * If non-blocking, use event to query the state of the transfer. @n
         * 
         * @param[out] buffer The buffer to store the data. Should be initialized and have size of at least size
         * @param[in] offset The offset of the cpu array at which the transfer starts
         * @param[in] size The size of the data to be trasfered
         * @param[in] cpuArray Pointer to the cpu array
         * @param[in] blocking CL_TRUE to wait for transfer to finish. CL_FALSE to continue without waiting
         * @param[out] event Event object to use in non-blocking mode to query the state of the transfer
         * @tparam T Type of data
         */
        template <typename T> static void readFromBuffer(const cl::Buffer& buffer, cl_uint offset,
            cl_uint size, T* cpuArray, cl_bool blocking = CL_TRUE, cl::Event* event = NULL);

        /**
         * \brief Transfer data between gpu buffers.
         * \details Transfer data between gpu buffers. @n
         * Blocking and non-blocking mode are available. @n
         * If non-blocking, use waitQueue() to wait for the transfer to finish. @n
         * 
         * @param[in] bsrc The source buffer from which the data will be transfered. Should be initialized and have size of at least size
         * @param[out] bdst The destination buffer to which the data will be transfered. Should be initialized and have size of at least size
         * @param[in] src_offset The offset of the bsrc at which the transfer starts
         * @param[in] dst_offset The offset of the bdst at which the transfer starts
         * @param[in] size The size of the data to be trasfered
         * @param[in] blocking CL_TRUE to wait for transfer to finish. CL_FALSE to continue without waiting
         * @tparam T Type of data
         */
        template <typename T> static void copyBetweenBuffers(cl::Buffer& bsrc, cl::Buffer& bdst, cl_uint src_offset,
            cl_uint dst_offset, cl_uint size, cl_bool blocking = CL_TRUE);

        /**
         * \brief Get size of gpu buffer.
         * 
         * @param[in] buf The buffer to query for its size. Should be initialized
         * @tparam T Type of data
         * @return Size of buffer
         */
        template <typename T> static cl_uint getBufferSize(const cl::Buffer& buf);

        /**
         * \brief Write gpu buffer to file. Buffer must be accessible from cpu (check mem_flags)
         * 
         * @param[in] filename The name of the file the data will be written to
         * @param[in] data The buffer from which the data will be written to the file. Should be initialized and have size at least dims1*dims2
         * @param[in] dim1 Number of lines to write to the file
         * @param[in] dim2 Number of rows to write to the file
         * @tparam T Type of data
         */
        template <typename T> static void writeToFile(std::string filename, cl::Buffer& data,
            cl_uint dim1, cl_uint dim2 = 1);

        /**
         * \brief Load a file's contents into a gpu buffer. Buffer must be accessible from cpu (check mem_flags)
         * 
         * @param[in] filename The name of the file the data will be taken from
         * @param[out] data The buffer to which the data will be written. Should be initialized and have size at least dims1*dims2
         * @param[in] dim1 Number of lines to write to the file
         * @param[in] dim2 Number of rows to write to the file
         * @tparam T Type of data
         */
        template <typename T> static void readFromFile(std::string filename, cl::Buffer& data,
            cl_uint dim1, cl_uint dim2 = 1);

        /**
         * \brief Loads the source code of a file into a string
         * @param filename The filename of the file to read its contents
         * @return The contents of the file
         */
        static std::string loadSource(std::string filename);

        /**
         * \brief Extends a number to the nearest multiple of multiple
         * \details Extends a number to the nearest multiple of multiple. @n
         * Useful when data is too small for gpu and should be extended to a multiple of hardware threads. @n
         * 
         * @param input The input number
         * @param multiple The multiple at which the result should fit
         * 
         * @return The new number which is a multiple of multiple
         */
        static cl_uint extendToMultiple(cl_uint input, cl_uint multiple);

        /**
         * \brief Get the device vendor
         * @return The #Vendor of the device
         */
        static Vendor getDeviceVendor();

        /**
         * \brief Print the error string corresponding to the error number
         * \details Print the error string corresponding to the error number. @n
         * Refers only to opencl error numbers, taken directly from cl.h header. @n
         * 
         * @param numError The error number
         * @return The error string
         */
        static std::string printError(cl_int numError);

        static std::vector<cl::Platform> platform; /**< \brief The desired OpenCL platform */
        static cl::Context context; /**< \brief The OpenCL context */
        static std::vector<cl::Device> device; /**< \brief The desired OpenCL device */
        static cl::CommandQueue queue; /**< \brief The OpenCL command queue */
        static Vendor vendor; /**< \brief The name of the device vendor */
        static cl_uint max_work_group_size; /**< \brief The max workgroup size of the device */
        static cl_uint local_mem_size; /**< \brief The total local memory size of the device */
        static cl_uint global_mem_size; /**< \brief The total global memory size of the device */
        static std::vector<std::string> device_extensions; /**< \brief The OpenCL extensions the device supports */


	private:

		std::vector<cl::Program> programs; /**< \brief The programs containing the kernels */
        std::vector<cl::Kernel> kernels; /**< \brief The kernel objects that relate to each kernel function inside a program */
		uint kernelIndex; /**< \brief An index used to specify the current kernel. Used in #setCurrentKernel */
        std::vector<uint> kernelArgCounters; /**< \brief Each item keeps the current index of a kernel. Used in #setCurrentKernel */
        cl::Event event; /**< \brief Event used for quering status of a command */
        
        static std::string printDeviceType(int type); /**< \brief Print the device type */
        static std::string printCacheType(int type); /**< \brief Print the device cache type */
        static std::string printMemoryType(int type); /**< \brief Print the memory type */


	protected:
        
        /**
         * \brief Generate the program and kernel objects. Every subclass should implement this method and fill the #programSources, #programParams and #kernelNames
         */
        virtual void setup(void) = 0;
        
        /**
         * \brief Compile the source code and add a program object into #programs
         * 
         * @param programSource The program's source code
         * @param params Parameters passed to the compiler
         */
		void loadProgram(std::string programSource, std::string params = "");

        /**
         * \brief Get the number of total kernels created
         * \@return The number of kernels
         */
        uint getNumKernels(void);

        /**
         * \brief Create a kernel object from its name and the program containing it
         * 
         * @param prog The program containing the kernel
         * @param kernelName The kernel name
         */
        void createKernel(const cl::Program& prog, std::string kernelName);

        /**
         * \brief Sets the current kernel. Used for less typing
         * 
         * @param[in] index The index from the #kernels array
         */
        void setCurrentKernel(uint index);

        /**
         * \brief Pass a buffer object as a global memory kernel argument
         * 
         * @param[in] arg The buffer to pass as kernel argument
         * @param[in] argindex The position of the argument in the kernel function. If not specified, it is taken from #kernelArgCounters
         */
        void setGlobalArg(cl::Buffer& arg, cl_int argindex = -1);

        /**
         * \brief Allocate a local memory kernel argument with the specified size. 
         * 
         * @param[in] local_size The size of the local memory to be allocated. Should NOT exceed #local_mem_size
         * @param[in] argindex The position of the argument in the kernel function. If not specified, it is taken from #kernelArgCounters
         */
        void setLocalArg(std::size_t local_size, cl_int argindex = -1);

        /**
         * \brief Pass a variable as a constant memory kernel argument
         * 
         * @param[in] arg The variable to pass as kernel argument
         * @param[in] argindex The position of the argument in the kernel function. If not specified, it is taken from #kernelArgCounters
         */
        void setConstantArg(cl_uint arg, cl_int argindex = -1);

        /**
         * \brief Run the kernel with the specified number of work-items
         * 
         * @param[in] global_size The number of total work-items
         * @param[in] local_size The number of work-items in a work-group. If specified, global_size should be a multiple of it. If not specified, device picks on its own
         * @param[in] wait Wait or not for the kernel to finish execution. If wait is false, use #waitEvent to wait for kernel to finish
         */
        void runKernel(int global_size, int local_size = 0, bool wait = true);

        /**
         * \brief Wait for all enqueued commands to finish before going on. Usefull when some methods are in non-blocking mode
         */
        static void waitQueue();

        /**
         * \brief Wait for the command bound to the event to finish. Usefull when #runKernel is in non-wait mode
         */
        void waitEvent();

		std::vector<std::string> programSources; /**< \brief The programs' source code */
		std::vector<std::string> programParams; /**< \brief The programs' parameters to be passed at compile time, i.e. preprocessor defines */
		std::vector<std::vector<std::string>> kernelNames; /**< \brief The kernel names of each program */
	};
}

#include "ompl/util/opencl/OpenCL.tcc"

#endif
