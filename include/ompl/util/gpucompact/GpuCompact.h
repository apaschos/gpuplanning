/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */

#ifndef OMPL_UTIL_GPUCOMPACT_
#define OMPL_UTIL_GPUCOMPACT_

#include "ompl/util/opencl/OpenCL.h"
#include "ompl/util/gpuscan/GpuScan.h"
#include "ompl/util/gpubufinit/GpuBufInit.h"


namespace ompl
{
    /** \brief Prefix Sum
     */
    class GpuCompact : public OpenCL
    {
    public:

        /** \brief Default Constructor */
        GpuCompact();

        void compact(cl::Buffer& d_Input, cl::Buffer& d_Diff, cl::Buffer& d_PSumDiff,
                cl::Buffer& d_Unique, const cl_uint columns, const cl_uint multiple,
                bool keepInitialSize = false);
//add more compact methods
        void unique(cl::Buffer& d_Input, cl::Buffer& d_Unique, bool keepInitialSize = false);
        void unique(cl::Buffer& d_Input, cl::Buffer& d_Unique, cl::Buffer& d_Start,
                cl::Buffer& d_Count, bool keepInitialSize = false);
        void uniqueByKeys(cl::Buffer& d_Input, cl::Buffer& d_Unique, const cl_uint columns,
                bool keepInitialSize = false);
        void uniqueByKeys(cl::Buffer& d_Input, cl::Buffer& d_Unique, cl::Buffer& d_Start,
                cl::Buffer& d_Count, const cl_uint columns, bool keepInitialSize = false);
        void multipleUnique(cl::Buffer& d_Input, cl::Buffer& d_Unique, const cl_uint multiple,
                bool keepInitialSize = false);
        void multipleUniqueByKeys(cl::Buffer& d_Input, cl::Buffer& d_Unique,
                const cl_uint columns, const cl_uint multiple, bool keepInitialSize = false);
        void unique(cl::Buffer& d_Input, cl::Buffer& d_Unique, cl::Buffer* d_Start,
                cl::Buffer* d_Count, const cl_uint columns, const cl_uint multiple,
                bool keepInitialSize = false);

        cl_uint getNumUniques();

    private:
        GpuScan sc;
        GpuBufInit bfn;

        cl_uint uniqs; // will hold the number of unique elements

        virtual void setup(void);
    };
}

#endif
