/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */

#ifndef OMPL_UTIL_GPUBUFINIT_
#define OMPL_UTIL_GPUBUFINIT_

#include "ompl/util/opencl/OpenCL.h"


namespace ompl
{
    /**
     * \file GpuBufInit.h
     * \author Andrew Paschos
     * \brief Header file for initializing buffer contents with specific value
     */
    class GpuBufInit : public OpenCL
    {
    public:

        /** \brief Default Constructor */
        GpuBufInit();

        /**
         * \brief Initialize an 1-d buffer
         *
         * Initialize an 1-d buffer with the specified value in the given range
         *
         * \param d_data the buffer to be filled with data
         * \param value the value to be written in the buffer
         * \param start the start index in buffer
         * \param end the end index in buffer
         */
        void initializeBuffer(cl::Buffer& d_data, cl_uint value, cl_int start = -1,
                cl_int end = -1);

        /**
         * \brief Initialize a 2-d buffer
         */
        void initializeBuffer64bit(cl::Buffer& d_data, cl_uint value1, cl_uint value2,
                cl_int start = -1, cl_int end = -1);

        /**
         * \brief Initialize a buffer (keys only)
         */
        void initializeBuffer_KeysOnly(cl::Buffer& d_data, cl_uint value, cl_uint columns,
                cl_int start = -1, cl_int end = -1);

        /**
         * \brief Initialize a 1-d buffer with linear initialization
         */
        void initializeBufferLinear(cl::Buffer& d_data, cl_int start = -1, cl_int end = -1);

        /**
         * \brief Initialize a 2-d buffer with linear initialization (keys only)
         */
        void initializeBufferLinear_KeysOnly(cl::Buffer& d_data, uint columns,
                cl_int start = -1, cl_int end = -1);


    private:
        /**
         * \brief Setup OpenCL kernels
         */
        virtual void setup(void);
    };
}

#endif
