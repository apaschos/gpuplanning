
/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */

#ifndef OMPL_UTIL_GPUROBINHOOD_
#define OMPL_UTIL_GPUROBINHOOD_

#include "ompl/util/gpurng/GpuRandomNumbers.h"
#include "ompl/util/gpuhash/GpuHash.h"
#include "ompl/util/gpubufinit/GpuBufInit.h"


namespace ompl
{
	class GpuRobinHood : public GpuHash
    {
    public:
    	GpuRobinHood(const cl_float load = 0.9, const cl_uint blocksize = 64);

        virtual int build(cl::Buffer& d_Input);
        virtual void retrieve(cl::Buffer& d_Keys, cl::Buffer& d_Values);
        void detectEmptyKeys(bool enabled);

        // Setters

        // Getters

    private:
        cl_uint blocksize;
        cl_uint load;
        cl_uint max_age;
        bool emptyKeys;

    	GpuBufInit in;
        RNG rng;

        cl::Buffer d_HashTable;
        cl::Buffer d_MAT; // Max Age Table
        cl::Buffer d_Offsets;
        cl::Buffer d_Alarm;
        
        virtual void setup(void);
	};
}

#endif
