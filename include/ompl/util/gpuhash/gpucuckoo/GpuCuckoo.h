/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */

#ifndef OMPL_UTIL_GPUCUCKOO_
#define OMPL_UTIL_GPUCUCKOO_

#include "ompl/util/gpurng/GpuRandomNumbers.h"
#include "ompl/util/gpuhash/GpuHash.h"
#include "ompl/util/gpuscan/GpuScan.h"
#include "ompl/util/gpubufinit/GpuBufInit.h"


namespace ompl
{
	class GpuCuckoo : public GpuHash
    {
    public:
    	GpuCuckoo(const cl_float bucketLoad = 0.8, const cl_float gamma = 0.8,
    			const cl_uint max_attempts = 50);

        virtual int build(cl::Buffer& d_Input);
        virtual void retrieve(cl::Buffer& d_Keys, cl::Buffer& d_Values);

        // Setters
        void setSeed1(cl_uint sd);
        void setSeed2(cl_uint sd);
        void setSubtables(cl_uint num);
        void setPrime(cl_uint prime);
        void setBucketLoadFactor(cl_float load);
        void setTotalLoadFactor(cl_float load);
        void setMaximumAttempts(cl_uint num);

        // Getters
        cl_uint getSeed1();
        cl_uint getSeed2();
        cl_uint getSubtables();
        cl_uint getPrime();
        cl_float getBucketLoadFactor();
        cl_float getTotalLoadFactor();
        cl_uint getMaximumAttempts();

    private:
        virtual void setup(void);

    	cl_float bucketLoad;
    	cl_float gamma;
    	cl_uint max_attempts;
    	cl_uint subtables;
    	cl_uint primenum;
    	cl_uint cuckoo1_seed;
    	cl_uint cuckoo2_seed;

    	GpuScan sc;
    	GpuBufInit in;
        RNG rng;

        cl::Buffer d_HashTable;
		cl::Buffer d_cuckoo1_Randoms; // c0, c1
		cl_uint buckets; // number of buckets - workgroups

		cl::Buffer d_cuckoo2_seeds; // seeds used for calculating the right bucket in each attempt (size: MAX_ATTEMPTS)
		cl::Buffer d_cuckoo2_constants; // constants used for xoring the corresponding seed (size: SUBTABLES * 2)
		cl_uint localTableSize; // size of the hash table for each workgroup
	};
}

#endif
