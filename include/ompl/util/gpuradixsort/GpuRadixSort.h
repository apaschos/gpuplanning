/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */

#ifndef OMPL_UTIL_GPURADIXSORT_
#define OMPL_UTIL_GPURADIXSORT_

#include "ompl/util/opencl/OpenCL.h"
#include "ompl/util/gpubufinit/GpuBufInit.h"
#include "ompl/util/gpuscan/GpuScan.h"


namespace ompl
{
	class GpuRadixSort : public OpenCL
    {
    public:
    	GpuRadixSort(const cl_uint chunk = 1,
    		const cl_uint bitsperpass = 2, const cl_uint totalbits = 32);

        void computeInversePermutation(cl::Buffer& d_Perm, cl::Buffer& d_InvPerm);

        void sort(cl::Buffer& d_Input);
        void sort(cl::Buffer& d_Input, cl::Buffer& d_Output);
        void sortByKeys(cl::Buffer& d_Input, const cl_uint columns);
        void sortByKeys(cl::Buffer& d_Input, cl::Buffer& d_Output, const cl_uint columns);

        void multipleSort(cl::Buffer& d_Input, const cl_uint multiple);
        void multipleSort(cl::Buffer& d_Input, cl::Buffer& d_Output, const cl_uint multiple);
        void multipleSortByKeys(cl::Buffer& d_Input, const cl_uint columns, const cl_uint multiple);
        void multipleSortByKeys(cl::Buffer& d_Input, cl::Buffer& d_Output,
                    const cl_uint columns, const cl_uint multiple);

        void sortPermute(cl::Buffer& d_Input, cl::Buffer& d_Perm);
        void sortPermute(cl::Buffer& d_Input, cl::Buffer& d_Output, cl::Buffer& d_Perm);
        void sortPermuteByKeys(cl::Buffer& d_Input, cl::Buffer& d_Perm, const cl_uint columns);
        void sortPermuteByKeys(cl::Buffer& d_Input, cl::Buffer& d_Output,
                    cl::Buffer& d_Perm, const cl_uint columns);

        void sort(cl::Buffer& d_Input, cl::Buffer* d_Output, cl::Buffer* d_Perm,
                    const cl_uint startIndex, const cl_uint rows, const cl_uint columns,
                    const cl_uint multiple);


        // Setters
        void setChunkSize(cl_uint ch);
        void setBitsPerPass(cl_uint b);
        void setTotalBits(cl_uint b);
        void setBuckets(cl_uint bucks);

        // Getters
        cl_uint getChunkSize();
        cl_uint getBitsPerPass();
        cl_uint getTotalBits();
        cl_uint getBuckets();

    private:
    	cl_uint totalbits;
	    cl_uint bits;
	    cl_uint buckets;
	    cl_uint chunk;

        GpuBufInit in;
        GpuScan sc;
        
        virtual void setup(void);
	};
}

#endif
