/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */


#ifndef OMPL_UTIL_GPURNG_GPU_RANDOM_NUMBERS_
#define OMPL_UTIL_GPURNG_GPU_RANDOM_NUMBERS_

#include "ompl/util/opencl/OpenCL.h"
#include <random>


namespace ompl
{
    /** \brief Random number generation.
        For the moment, only uniform is available. */
    class GpuRNG : public OpenCL
    {
    public:

        /** \brief Constructor. Always sets a different random seed */
        GpuRNG(cl_uint dim1, cl_uint dim2, cl_uint generator = 0);

        void generateRandomData(cl::Buffer& data);
        void setSeed(cl_uint seed);
        cl_uint getSeed();

        void setBounds(std::vector<cl_int> lower_bounds, std::vector<cl_int> upper_bounds);
        void setBounds(cl_int lower_bound, cl_int upper_bound);
        void setBounds(cl_int* lower_bounds, cl_int* upper_bounds);

    private:

    	cl_uint	generator;
        cl_uint dim1;
        cl_uint dim2;
        bool boundsSet;
        bool seedSet;

        cl::Buffer d_lower_bounds;
        cl::Buffer d_range;

        virtual void setup(void);
    };




    class RNG
    {
    public:

        RNG(cl_uint generator = 0);

        void setSeed(cl_uint seed = 0);

        cl_uint getSeed();

        template <typename T>
        void generateUniformNumbers(T* output, uint total, uint lower_limit, uint upper_limit);
        template <typename T>
        void generateGaussianNumbers(T* output, uint total, float mean, float standard_deviation,
                bool absolute = false);

    private:

        cl_uint generator;
        std::mt19937 rng;
    };
}

#include "ompl/util/gpurng/GpuRandomNumbers.tcc"

#endif
