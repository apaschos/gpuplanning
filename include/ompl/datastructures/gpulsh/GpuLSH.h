/*********************************************************************
* Software License Agreement (BSD License)
*
*  Copyright (c) 2014, University of Patras
*  All rights reserved.
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above
*     copyright notice, this list of conditions and the following
*     disclaimer in the documentation and/or other materials provided
*     with the distribution.
*   * Neither the name of the Willow Garage nor the names of its
*     contributors may be used to endorse or promote products derived
*     from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
*  POSSIBILITY OF SUCH DAMAGE.
*********************************************************************/

/* Author: Andrew Paschos */

#ifndef OMPL_UTIL_GPULSH_
#define OMPL_UTIL_GPULSH_

#include "ompl/util/gpurng/GpuRandomNumbers.h"
#include "ompl/util/opencl/OpenCL.h"
#include "ompl/util/gpuhash/gpucuckoo/GpuCuckoo.h"
#include "ompl/util/gpuradixsort/GpuRadixSort.h"
#include "ompl/util/gpucompact/GpuCompact.h"
#include "ompl/util/gpubufinit/GpuBufInit.h"


namespace ompl
{
    typedef struct
    {
        cl_uint dimensions;
        std::vector<cl_float> weights;
    } config;


    /** \brief Locality Sensitive Hashing
     */
    class GpuLSH : public OpenCL
    {
    public:
        GpuLSH(const config robot, const cl_uint K, const cl_uint L = 4,
                const cl_uint M = 3, const cl_uint W = 1000);

        void calculateLSH(cl::Buffer& d_Input, cl::Buffer& d_Output, uint seed);
        void nearestK(cl::Buffer& d_Input, cl::Buffer& d_KNN);

        void setDelta(cl_float num);

    private:
        cl_uint dimensions_;
        cl_uint K_;
        cl_uint L_;
        cl_uint M_;
        cl_uint W_;
        cl_float delta;
        cl_uint extraK;

        cl::Buffer d_Weights;
        cl::Buffer d_a;
        cl::Buffer d_b;
        cl::Buffer d_r;
        std::vector<cl_uint> seeds;

        GpuBufInit bfn;
        GpuRadixSort rdx;
        GpuCompact unq;
        GpuCuckoo cck;
        RNG rng;
        
        virtual void setup(void);
    };
}

#endif
